package de.myzelyam.playerfights;

import de.myzelyam.playerfights.duel.DuelArena;
import de.myzelyam.playerfights.duel.WinAnnouncementMessages;
import me.konsolas.aac.api.PlayerViolationCommandEvent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class AACHook implements Listener {

	private PlayerFights plugin;

	public AACHook(PlayerFights plugin) {
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPunish(PlayerViolationCommandEvent e) {
		Player p = e.getPlayer();
		if (!(plugin.getArenaMgr().getArena(p) instanceof DuelArena))
			return;
		DuelArena a = (DuelArena) plugin.getArenaMgr().getArena(p);
		if (a == null)
			return;
		if (!(e.getCommand().toLowerCase().contains("aackick")
				|| e.getCommand().toLowerCase().contains("aaccrash")
				|| e.getCommand().toLowerCase().contains("tempban")
				|| e.getCommand().toLowerCase().contains("ban")
				|| e.getCommand().toLowerCase().contains("kick")))
			return;
		a.sendArenaMessageWithPrefix(a.getOtherPlayer(p).getName()
				+ "" + ChatColor.GOLD + " won round " + ChatColor.YELLOW + "" + (a.getRound() - 1) + "" + ChatColor.GOLD + " with " + ChatColor.YELLOW + ""
				+ a.getOtherPlayer(p).getHealth() + "" + ChatColor.GOLD + "hp left!");
		a.sendArenaMessageWithPrefix("" + ChatColor.RED + "" + p.getName()
				+ "" + ChatColor.GOLD + " has been kicked out of the arena for hacking.");
		WinAnnouncementMessages.announce(a.getOtherPlayer(p), p,
				(int) p.getHealth(), a);
		a.forceEnd(ArenaStopReason.FORCED_STOP);
	}
}
