package de.myzelyam.playerfights;

import org.bukkit.ChatColor;

import java.util.concurrent.TimeUnit;

public abstract class GeneralUtils {

    public static String getDurationBreakdown(long seconds) {
        if (seconds <= 60)
            return seconds + "" + ChatColor.YELLOW + " seconds";
        long minutes = TimeUnit.SECONDS.toMinutes(seconds);
        seconds -= minutes * 60;
        return String.valueOf(minutes) + "m " + seconds + "s";
    }
}
