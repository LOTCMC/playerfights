package de.myzelyam.playerfights;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class PlayerDataMgr {

	private final PlayerFights plugin;

	private final Map<Player, ItemStack[]> storedInventories = new HashMap<>();

	private final Map<Player, ItemStack> storedHelmet = new HashMap<>(),
			storedChestPlate = new HashMap<>(),
			storedLeggings = new HashMap<>(), storedBoots = new HashMap<>();

	private final Map<Player, Integer> storedLevel = new HashMap<>();

	private final Map<Player, Integer> storedFood = new HashMap<>();

	private final Map<Player, Collection<PotionEffect>> storedPotionEffects = new HashMap<>();

	private final Map<Player, Location> storedLocation = new HashMap<>();

	public PlayerDataMgr(PlayerFights plugin) {
		this.plugin = plugin;
	}

	public void savePlayerData(Player p) {
		if (p == null)
			return;
		storedInventories.put(p, p.getInventory().getContents());
		storedHelmet.put(p, p.getInventory().getHelmet());
		storedChestPlate.put(p, p.getInventory().getChestplate());
		storedLeggings.put(p, p.getInventory().getLeggings());
		storedBoots.put(p, p.getInventory().getBoots());
		storedLevel.put(p, p.getLevel());
		storedPotionEffects.put(p, p.getActivePotionEffects());
		storedFood.put(p, p.getFoodLevel());
		storedLocation.put(p, p.getLocation());
	}

	public void remKey(Player p) {
		storedInventories.remove(p);
		storedHelmet.remove(p);
		storedChestPlate.remove(p);
		storedLeggings.remove(p);
		storedBoots.remove(p);
		storedLevel.remove(p);
		storedPotionEffects.remove(p);
		storedFood.remove(p);
		storedLocation.remove(p);
	}

	public void restorePlayerData(Player p) {
		if (storedInventories.get(p) != null && storedLevel.get(p) != null
				&& storedPotionEffects.get(p) != null) {
			p.getActivePotionEffects().clear();
			p.getInventory().clear();
			p.getInventory().setContents(storedInventories.get(p));
			p.getInventory().setHelmet(storedHelmet.get(p));
			p.getInventory().setChestplate(storedChestPlate.get(p));
			p.getInventory().setLeggings(storedLeggings.get(p));
			p.getInventory().setBoots(storedBoots.get(p));
			p.setLevel(storedLevel.get(p));
			p.addPotionEffects(storedPotionEffects.get(p));
			p.setFoodLevel(storedFood.get(p));
			p.teleport(storedLocation.get(p), TeleportCause.PLUGIN);
			remKey(p);
		}
	}

	public PlayerFights getPlugin() {
		return plugin;
	}
}
