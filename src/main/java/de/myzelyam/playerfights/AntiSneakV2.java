package de.myzelyam.playerfights;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.*;

public abstract class AntiSneakV2 {

	public static void registerListener(final PlayerFights plugin2) {
		ProtocolLibrary.getProtocolManager().addPacketListener(
				new PacketAdapter(plugin2, ListenerPriority.NORMAL,
						PacketType.Play.Server.ENTITY_METADATA) {

					@Override
					public void onPacketSending(PacketEvent event) {
						Player receiver = event.getPlayer();
						if (plugin2.getArenaMgr().getArena(receiver) != null
								&& plugin2.getArenaMgr().getArena(receiver)
										.getState() == ArenaState.INGAME) {
							Entity entitySender = ProtocolLibrary
									.getProtocolManager()
									.getEntityFromID(receiver.getWorld(), event
											.getPacket().getIntegers().read(0));
							if (entitySender instanceof Player) {
								Player sender = (Player) entitySender;
								if (sender.isSneaking())
									event.setCancelled(true);
							}
						}
					}
				});
	}
}
