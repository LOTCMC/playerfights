package de.myzelyam.playerfights;

import de.myzelyam.playerfights.duel.DuelArena;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class ChallengeMgr {

    private final PlayerFights plugin;

    private Map<Challenge, Integer> pendingChallenges = new HashMap<>();

    private Map<Challenge, Integer> amountOfFights = new HashMap<>();

    public List<Player> preTeleportPlayers = new LinkedList<>();

    public ChallengeMgr(PlayerFights plugin) {
        this.plugin = plugin;
        startTask();
    }

    private void startTask() {
        new BukkitRunnable() {

            @Override
            public void run() {
                for (Challenge c : new ArrayList<>(
                        pendingChallenges.keySet())) {
                    if (pendingChallenges.get(c) > 60) {
                        pendingChallenges.remove(c);
                        c.from.sendMessage(plugin.getPrefix(c.type)
                                + "" + ChatColor.YELLOW + "Your pending challenge to "
                                + c.to.getName() + "" + ChatColor.YELLOW + " expired.");
                        continue;
                    }
                    pendingChallenges.put(c, pendingChallenges.get(c) + 1);
                }
            }
        }.runTaskTimer(plugin, 0, 20);
    }

    public boolean hasPendingChallenge(Player p) {
        for (Challenge c : pendingChallenges.keySet()) {
            if (c.to.getUniqueId().toString()
                    .equals(p.getUniqueId().toString())) {
                return true;
            }
        }
        return false;
    }

    public boolean addChallenge(Player to, Player from, ArenaType type,
                                int amountOfFights) {
        for (Challenge c : pendingChallenges.keySet()) {
            if (c.to.getUniqueId().toString()
                    .equals(to.getUniqueId().toString())
                    && c.from.getUniqueId().toString()
                    .equals(from.getUniqueId().toString())) {
                from.sendMessage(plugin.getPrefix(type)
                        + "" + ChatColor.RED + "Please wait for the expiration of your current challenge.");
                return false;
            }
        }
        Challenge c = new Challenge(from, to, type);
        pendingChallenges.put(c, 0);
        this.amountOfFights.put(c, amountOfFights);
        return true;
    }

    public void denyChallenge(Player to, Player from, boolean block) {
        for (Challenge c : new ArrayList<>(
                pendingChallenges.keySet())) {
            if (c.to.getUniqueId().toString()
                    .equals(to.getUniqueId().toString())
                    && c.from.getUniqueId().toString()
                    .equals(from.getUniqueId().toString())) {
                pendingChallenges.remove(c);
                from.sendMessage(plugin.getPrefix(c.type)
                        + "" + ChatColor.RED + "Your pending challenge to " + c.to.getName()
                        + " " + ChatColor.RED + "has been denied!");
                if (!block)
                    to.sendMessage(plugin.getPrefix(c.type) + "" + ChatColor.YELLOW + "You denied "
                            + from.getName() + "" + ChatColor.YELLOW + "'s challenge!");
                else
                    to.sendMessage(plugin.getPrefix(c.type) + "" + ChatColor.YELLOW + "You denied "
                            + from.getName()
                            + "" + ChatColor.YELLOW + "'s challenge and blocked prospective challenges from that player! You can use " + ChatColor.GOLD + "\"/duel unblock "
                            + c.from.getName() + "\"" + ChatColor.YELLOW + " to undo this.");
            }
        }
    }

    public void acceptChallenge(final Player to, final Player from) {
        for (final Challenge c : new ArrayList<>(
                pendingChallenges.keySet())) {
            if (c.to.getUniqueId().toString()
                    .equals(to.getUniqueId().toString())
                    && c.from.getUniqueId().toString()
                    .equals(from.getUniqueId().toString())) {
                pendingChallenges.remove(c);
                from.sendMessage(plugin.getPrefix(c.type)
                        + ChatColor.GREEN + "Your pending challenge to " + c.to.getName()
                        + " " + ChatColor.GREEN + "has been accepted! Teleporting in 5 seconds...");
                to.sendMessage(plugin.getPrefix(c.type) + "" + ChatColor.YELLOW + "You accepted " + from.getName()
                        + ChatColor.YELLOW + "'s challenge! Teleporting in 5 seconds...");
                final ArenaMgr am = plugin.getArenaMgr();
                preTeleportPlayers.add(to);
                preTeleportPlayers.add(from);
                new BukkitRunnable() {

                    @Override
                    public void run() {
                        if (!preTeleportPlayers.contains(to)
                                || !preTeleportPlayers.contains(from)) {
                            String msg = plugin.getPrefix(c.type)
                                    + "" + ChatColor.RED + "Cancelled teleportation and duel since one player was damaged,"
                                    + " please try again.";
                            to.sendMessage(msg);
                            from.sendMessage(msg);
                            preTeleportPlayers.remove(to);
                            preTeleportPlayers.remove(from);
                            amountOfFights.remove(c);
                            return;
                        }
                        preTeleportPlayers.remove(to);
                        preTeleportPlayers.remove(from);
                        if (((to.isOnline() && from.isOnline()
                                && !am.isInGame(to) && !am.isInGame(from)
                                && isWorldAllowed(to, from))
                                && !(plugin.getServer().getPluginManager()
                                .isPluginEnabled("PvPManager")
                                && (PvPMgrHook.isInCombat(to)
                                || PvPMgrHook
                                .isInCombat(from))))
                                && (!(Bukkit.getPluginManager()
                                .isPluginEnabled("PremiumVanish")
                                && (PVHook.isVanished(to)
                                || PVHook.isVanished(from))))) {
                            DuelArena a = am.getRandomDuelArena();
                            if (a == null) {
                                String msg = plugin.getPrefix(c.type)
                                        + ChatColor.RED + "Cancelled teleportation and duel since there is no " +
                                        "duel arena available currently, please try again later.";
                                to.sendMessage(msg);
                                from.sendMessage(msg);
                                amountOfFights.remove(c);
                                return;
                            }
                            a.addPlayers(c.from, c.to, amountOfFights.get(c));
                            amountOfFights.remove(c);
                        } else {
                            String msg = plugin.getPrefix(c.type)
                                    + ChatColor.RED + "Cancelled teleportation and duel since one player left the server,"
                                    + " joined an other arena, is in a blacklisted world or is in combat.";
                            to.sendMessage(msg);
                            from.sendMessage(msg);
                            amountOfFights.remove(c);
                        }
                    }

                    private boolean isWorldAllowed(Player to, Player from) {
                        boolean toOK = false, fromOK = false;
                        for (String world : plugin.getConfig()
                                .getStringList("WhitelistedJoinWorlds")) {
                            if (to.getWorld().getName()
                                    .equalsIgnoreCase(world)) {
                                toOK = true;
                            }
                            if (from.getWorld().getName()
                                    .equalsIgnoreCase(world)) {
                                fromOK = true;
                            }
                        }
                        return toOK && fromOK;
                    }
                }.runTaskLater(plugin, 20 * 5);
            }
        }
    }

    public void blockChallenge(Player blocked, Player from, ArenaType type) {
        denyChallenge(from, blocked, true);
        plugin.getDataMgr().blockPlayer(blocked.getUniqueId(),
                from.getUniqueId(), type);
    }

    public boolean unblockChallenge(Player blocked, Player from,
                                    ArenaType type) {
        if (!isBlocked(blocked, from, type))
            return false;
        plugin.getDataMgr().unblockPlayer(blocked.getUniqueId(),
                from.getUniqueId(), type);
        return true;
    }

    public boolean isBlocked(Player blocked, Player from, ArenaType type) {
        return plugin.getDataMgr().isBlocked(blocked.getUniqueId(),
                from.getUniqueId(), type);
    }

    public class Challenge {

        public Challenge(Player from, Player to, ArenaType type) {
            this.to = to;
            this.from = from;
            this.type = type;
        }

        public Player from;

        public Player to;

        public ArenaType type;
    }
}
