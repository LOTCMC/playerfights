package de.myzelyam.playerfights;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class PlayerFreezer {

    private final List<Player> frozenPlayers;

    public PlayerFreezer(final PlayerFights plugin, final Arena a) {
        frozenPlayers = new LinkedList<>();
        // register events
        plugin.getServer().getPluginManager().registerEvents(new Listener() {

            @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
            public void onMove(PlayerMoveEvent e) {
                Player p = e.getPlayer();
                // only affect in-game players
                if (plugin.getArenaMgr().getArena(p) != a)
                    return;
                // check if frozen
                if (frozenPlayers.contains(p)) {
                    if (e.getFrom().getBlockX() != e.getTo().getBlockX()
                            || e.getFrom().getBlockY() != e.getTo().getBlockY()
                            || e.getFrom().getBlockZ() != e.getTo().getBlockZ()) {
                        e.setTo(e.getFrom());
                    }
                }
            }

            @EventHandler(priority = EventPriority.MONITOR)
            public void onQuit(PlayerQuitEvent e) {
                Player p = e.getPlayer();
                removePlayer(p);
            }
        }, plugin);
    }

    public void addPlayer(Player... players) {
        frozenPlayers.addAll(Arrays.asList(players));
    }

    public void addPlayer(Collection<Player> players) {
        frozenPlayers.addAll(players);
    }

    public void removePlayer(Player p) {
        frozenPlayers.remove(p);
    }

    public void removeAll() {
        frozenPlayers.clear();
    }
}
