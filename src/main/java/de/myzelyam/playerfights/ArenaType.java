package de.myzelyam.playerfights;

public enum ArenaType {
	DUEL(
			"Duel");

	public static ArenaType fromString(String str) {
		if ("Duel".equalsIgnoreCase(str))
			return DUEL;
		return null;
	}

	private final String nice;

	ArenaType(String nice) {
		this.nice = nice;
	}

	public String makeNice() {
		return nice;
	}
}
