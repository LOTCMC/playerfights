package de.myzelyam.playerfights.commands;

import de.myzelyam.playerfights.PlayerLeaveReason;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ArenaLeave extends Command {

    @Override
    protected void execute(CommandSender sender, String[] args) {
        if (canDo(sender, CommandType.ARENA_LEAVE)) {
            Player p = (Player) sender;
            if (!am.isInGame(p)) {
                p.sendMessage(ChatColor.RED + "You aren't in an arena!");
                return;
            }
            am.getArena(p).removePlayer(p, PlayerLeaveReason.PLAYER_LEAVE);
        }
    }
}
