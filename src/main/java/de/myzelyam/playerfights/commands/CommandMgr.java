package de.myzelyam.playerfights.commands;

import de.myzelyam.playerfights.PlayerFights;
import de.myzelyam.playerfights.commands.setup.*;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class CommandMgr {

    private PlayerFights plugin;

    public CommandMgr(PlayerFights plugin) {
        this.setPlugin(plugin);
    }

    public void execute(org.bukkit.command.Command cmd, CommandSender sender,
                        String[] args) {
        if (cmd.getName().equalsIgnoreCase("duel")) {
            if (args.length == 1 && args[0].equalsIgnoreCase("leave")) {
                Command cmdAction = new ArenaLeave();
                cmdAction.execute(sender, args);
                return;
            }
            Command cmdAction = new DuelChallenge();
            cmdAction.execute(sender, args);
            return;
        }
        if (cmd.getName().equalsIgnoreCase("pfsetup")) {
            String usage = "/pfsetup <create|delete|check|toggle|addkit|removekit|editkit|" +
                    "addmap|removemap|list|addspawnloc|clearspawnlocs> [args]";
            if (args.length == 0) {
                sender.sendMessage(ChatColor.RED + "Invalid usage! " + usage);
                return;
            }
            String action = args[0];
            if (action.equalsIgnoreCase("create")) {
                new SetupCreate().execute(sender, args);
            } else if (action.equalsIgnoreCase("delete")) {
                new SetupDelete().execute(sender, args);
            } else if (action.equalsIgnoreCase("check")
                    || action.equalsIgnoreCase("state")
                    || action.equalsIgnoreCase("info")) {
                new SetupCheck().execute(sender, args);
            } else if (action.equalsIgnoreCase("toggle")
                    || action.equalsIgnoreCase("enable")
                    || action.equalsIgnoreCase("disable")
                    || action.equalsIgnoreCase("finish")) {
                new SetupToggle().execute(sender, args);
            } else if (action.equalsIgnoreCase("list")
                    || action.equalsIgnoreCase("players")
                    || action.equalsIgnoreCase("listplayers")
                    || action.equalsIgnoreCase("lipl")) {
                new SetupList().execute(sender, args);
            } else if (action.equalsIgnoreCase("addspawnloc")) {
                new SetupAddSpawnLoc().execute(sender, args);
            } else if (action.equalsIgnoreCase("clearspawnlocs")) {
                new SetupClearSpawnLocs().execute(sender, args);
            } else if (action.equalsIgnoreCase("addmap")) {
                new SetupAddMap().execute(sender, args);
            } else if (action.equalsIgnoreCase("removemap")) {
                new SetupRemoveMap().execute(sender, args);
            } else if (action.equalsIgnoreCase("setmap")) {
                new SetupSetMap().execute(sender, args);
            } else if (action.equalsIgnoreCase("addkit")) {
                new SetupAddKit().execute(sender, args);
            } else if (action.equalsIgnoreCase("removekit")) {
                new SetupRemoveKit().execute(sender, args);
            } else if (action.equalsIgnoreCase("editkit")) {
                new SetupEditKit().execute(sender, args);
            } else if (action.equalsIgnoreCase("setkiticon")) {
                new SetupSetKitIcon().execute(sender, args);
            } else if (action.equalsIgnoreCase("reload")
                    || action.equalsIgnoreCase("rl")) {
                new SetupReload().execute(sender, args);
            } else {
                sender.sendMessage(ChatColor.RED + "Invalid usage! " + usage);
            }
        }
    }

    public PlayerFights getPlugin() {
        return plugin;
    }

    private void setPlugin(PlayerFights plugin) {
        this.plugin = plugin;
    }
}
