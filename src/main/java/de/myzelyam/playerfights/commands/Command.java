package de.myzelyam.playerfights.commands;

import de.myzelyam.playerfights.ArenaMgr;
import de.myzelyam.playerfights.PlayerFights;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class Command {

    protected final PlayerFights plugin = PlayerFights.getInstance();

    protected final ArenaMgr am = plugin.getArenaMgr();

    protected abstract void execute(CommandSender sender, String[] args);

    protected boolean canDo(CommandSender p, CommandType cmd) {
        if (!(p instanceof Player))
            if (!cmd.canConsole()) {
                p.sendMessage(ChatColor.RED + "You must be a player to execute this command.");
                return false;
            }
        if (!p.hasPermission(cmd.getPerm()) && !cmd.getPerm().equalsIgnoreCase("/")) {
            p.sendMessage(ChatColor.DARK_RED + "Denied access! You are not allowed to do this.");
            return false;
        }
        return true;
    }

    protected enum CommandType {
        DUEL_CHALLENGE(
                "duel.challenge",
                false,
                "/duel <<Player>|accept|deny|block|unblock> [Player]",
                "Challenges a player to duel with you"),
        ARENA_LEAVE("/", false, "/<arenaType> leave", "Leaves the arena"),
        SETUP_CREATE(
                "pfsetup.create",
                true,
                "/pfsetup create <arena> <type>",
                "Creates a new arena"),
        SETUP_DELETE(
                "pfsetup.delete",
                true,
                "/pfsetup delete <arena>",
                "Deletes an existing arena"),
        SETUP_SETMAP(
                "pfsetup.setmap",
                true,
                "/pfsetup setmap <arena> <map>",
                "Sets the map for an arena"),
        SETUP_ADDMAP(
                "pfsetup.addmap",
                true,
                "/pfsetup addmap <map>",
                "Adds a virtual map with the block in your hand as icon"),
        SETUP_REMOVEMAP(
                "pfsetup.removemap",
                true,
                "/pfsetup removemap <map>",
                "Removes an existing virtual map"),
        SETUP_ADDSPAWNLOC(
                "pfsetup.addspawnloc",
                false,
                "/pfsetup addspawnloc <arena>",
                "Adds the location you're standing on to the arena's spawn locations"),
        SETUP_CLEARSPAWNLOCS(
                "pfsetup.clearspawnlocs",
                true,
                "/pfsetup clearspawnlocs <arena>",
                "Removes all spawn locations for an arena"),
        SETUP_CHECK(
                "pfsetup.check",
                true,
                "/pfsetup check <arena>",
                "Shows information about an arena"),
        SETUP_TOGGLE(
                "pfsetup.toggle",
                true,
                "/pfsetup toggle <arena>",
                "Enables or disables an arena"),
        SETUP_LIST(
                "pfsetup.list",
                true,
                "/pfsetup list",
                "Lists all arenas"),
        SETUP_ADDKIT(
                "pfsetup.addkit",
                false,
                "/pfsetup addkit <kit>",
                "Creates a new kit using the items in your inventory"),
        SETUP_REMOVEKIT(
                "pfsetup.removekit",
                true,
                "/pfsetup removekit <kit>",
                "Removes an existing kit"),
        SETUP_EDITKIT(
                "pfsetup.editkit",
                false,
                "/pfsetup editkit <inv|armor|offhand> <kit>",
                "Edits an existing kit"),
        SETUP_SETKITICON(
                "pfsetup.setkiticon",
                false,
                "/pfsetup setkiticon <kit>",
                "Sets the item in your hand to the icon of the kit"),
        SETUP_RELOAD(
                "pfsetup.reload",
                true,
                "/pfsetup reload",
                "Reloads the data file");

        private String perm;

        private boolean console;

        private String usage;

        private String description;

        CommandType(String perm, boolean console, String usage,
                    String description) {
            this.perm = perm;
            this.console = console;
            this.usage = usage;
            this.description = description;
        }

        public String getUsage() {
            return usage;
        }

        String getPerm() {
            return perm;
        }

        boolean canConsole() {
            return console;
        }

        String getDescription() {
            return description;
        }
    }
}
