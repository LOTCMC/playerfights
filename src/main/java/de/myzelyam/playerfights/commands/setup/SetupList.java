package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.Arena;
import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetupList extends Command {

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (canDo(sender, CommandType.SETUP_LIST)) {
            if (args.length == 2) {
                // mode 1: List players in arena
                String arenaName = args[1];
                Arena a = am.getArena(arenaName);
                if (a == null) {
                    sender.sendMessage("" + ChatColor.RED + "Invalid arena name.");
                    return;
                }
                String players = "";
                for (Player p : a)
                    players += p.getName() + ", ";
                players = players.substring(0, players.length() - 2);
                sender.sendMessage("" + ChatColor.YELLOW + "Players in arena '" + a.getDisplayId()
                        + "': " + players);
            } else {
                sender.sendMessage("" + ChatColor.GOLD + "Listing all arenas...");
                // mode 2: List arenas
                if (am.getAllArenas().isEmpty())
                    sender.sendMessage(
                            "" + ChatColor.RED + "No arenas found. Use '/pfsetup create <arena> <type>'");
                else
                    for (Arena a : am.getAllArenas()) {
                        sender.sendMessage("" + ChatColor.YELLOW + "" + a.getDisplayId() + " (" + ChatColor.RED + ""
                                + a.getType().makeNice() + "" + ChatColor.YELLOW + "): " + ChatColor.AQUA + ""
                                + a.getState());
                    }
            }
        }
    }
}