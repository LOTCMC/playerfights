package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.Arena;
import de.myzelyam.playerfights.ArenaState;
import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class SetupClearSpawnLocs extends Command {

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (canDo(sender, CommandType.SETUP_CLEARSPAWNLOCS)) {
			Player p = (Player) sender;
			if (args.length != 2) {
				p.sendMessage("" + ChatColor.RED + "Invalid usage! "
						+ CommandType.SETUP_CLEARSPAWNLOCS.getUsage());
				return;
			}
			String arenaName = args[1];
			Arena a = am.getArena(arenaName);
			if (a == null) {
				p.sendMessage("" + ChatColor.RED + "Invalid arena name.");
				return;
			}
			ArenaState state = a.getState();
			boolean enabled = state != ArenaState.DISABLED;
			if (enabled) {
				p.sendMessage(
						"" + ChatColor.RED + "You have to disable the arena first to do this! '/pfsetup toggle "
								+ a.getDisplayId() + "'");
				return;
			}
			plugin.getDataMgr().setSpawnLocsForArena(a.getId(),
					new ArrayList<Location>(0));
			List<Location> locs = a.getLocations();
			locs.clear();
			p.sendMessage(
					"" + ChatColor.GREEN + "Successfully cleared all spawn locations of the arena '"
							+ a.getDisplayId() + "'!");
		}
	}
}
