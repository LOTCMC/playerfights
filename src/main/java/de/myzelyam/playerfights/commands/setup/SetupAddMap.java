package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class SetupAddMap extends Command {

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (canDo(sender, CommandType.SETUP_ADDMAP)) {
			if (args.length != 2) {
				sender.sendMessage("" + ChatColor.RED + "Invalid usage! "
						+ CommandType.SETUP_ADDMAP.getUsage());
				return;
			}
			String map = args[1].toLowerCase();
			if (plugin.getDataMgr().getMapIDs().contains(map)) {
				sender.sendMessage("" + ChatColor.RED + "Invalid map name.");
				return;
			}
			plugin.getDataMgr().createMap(map, null);
			sender.sendMessage("" + ChatColor.GREEN + "Successfully created the map '" + map + "'!");
		}
	}

}
