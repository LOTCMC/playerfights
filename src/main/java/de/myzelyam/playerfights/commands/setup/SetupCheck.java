package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.Arena;
import de.myzelyam.playerfights.ArenaState;
import de.myzelyam.playerfights.ArenaType;
import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class SetupCheck extends Command {

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (canDo(sender, CommandType.SETUP_CHECK)) {
			if (args.length != 2) {
				sender.sendMessage("" + ChatColor.RED + "Invalid usage! "
						+ CommandType.SETUP_CHECK.getUsage());
				return;
			}
			String arenaName = args[1];
			Arena a = am.getArena(arenaName);
			if (a == null) {
				sender.sendMessage("" + ChatColor.RED + "Invalid arena name.");
				return;
			}
			ArenaState state = a.getState();
			boolean enabled = state != ArenaState.DISABLED;
			ArenaType type = a.getType();
			String name = a.getDisplayId();
			String id = a.getId();
			String[] checkCriterions = a.getCheckCriterions();
			sender.sendMessage(
					new String[] {
							"" + ChatColor.GOLD + "<" + ChatColor.YELLOW + "-------------< " + ChatColor.GOLD + "INFO" + ChatColor.YELLOW + " >-------------" + ChatColor.GOLD + ">",
							"" + ChatColor.AQUA + "General: " + ChatColor.RED + "" + name + "(" + id + ") ("
									+ type.toString() + ")",
							"" + ChatColor.AQUA + "Arena State: " + ChatColor.RED + "" + state.toString(),
							"" + ChatColor.AQUA + "Ready for use: " + ChatColor.RED + "" + a.isReadyForUse()
									+ " (Enabled: " + enabled + ")",
					"" + ChatColor.AQUA + "Setup Information: " + ChatColor.RED + "", });
			sender.sendMessage(checkCriterions);
		}
	}
}
