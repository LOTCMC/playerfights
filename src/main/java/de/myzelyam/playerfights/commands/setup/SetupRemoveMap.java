package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class SetupRemoveMap extends Command {

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (canDo(sender, CommandType.SETUP_REMOVEMAP)) {
			if (args.length != 2) {
				sender.sendMessage("" + ChatColor.RED + "Invalid usage! "
						+ CommandType.SETUP_REMOVEMAP.getUsage());
				return;
			}
			String map = args[1].toLowerCase();
			if (!plugin.getDataMgr().getMapIDs().contains(map)) {
				sender.sendMessage("" + ChatColor.RED + "Invalid map name.");
				return;
			}
			plugin.getDataMgr().deleteMap(map);
			sender.sendMessage("" + ChatColor.GREEN + "Successfully removed the map '" + map + "'!");
		}
	}

}
