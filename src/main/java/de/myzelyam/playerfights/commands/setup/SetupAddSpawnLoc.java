package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.Arena;
import de.myzelyam.playerfights.ArenaState;
import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

public class SetupAddSpawnLoc extends Command {

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (canDo(sender, CommandType.SETUP_ADDSPAWNLOC)) {
			Player p = (Player) sender;
			if (args.length != 2) {
				p.sendMessage("" + ChatColor.RED + "Invalid usage! "
						+ CommandType.SETUP_ADDSPAWNLOC.getUsage());
				return;
			}
			String arenaName = args[1];
			Arena a = am.getArena(arenaName);
			if (a == null) {
				p.sendMessage("" + ChatColor.RED + "Invalid arena name.");
				return;
			}
			ArenaState state = a.getState();
			boolean enabled = state != ArenaState.DISABLED;
			if (enabled) {
				p.sendMessage(
						"" + ChatColor.RED + "You have to disable the arena first to do this! '/pfsetup toggle "
								+ a.getDisplayId() + "'");
				return;
			}
			plugin.getDataMgr().addSpawnLocForArena(a.getId(), p.getLocation());
			List<Location> locs = a.getLocations();
			if (locs == null)
				locs = new LinkedList<>();
			locs.add(p.getLocation());
			a.setLocations(locs);
			p.sendMessage("" + ChatColor.GREEN + "Successfully added a spawn location to the arena '"
					+ a.getDisplayId() + "'!");
		}
	}
}
