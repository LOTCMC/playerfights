package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class SetupDelete extends Command {

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (canDo(sender, CommandType.SETUP_DELETE)) {
			if (args.length != 2) {
				sender.sendMessage("" + ChatColor.RED + "Invalid usage! "
						+ CommandType.SETUP_DELETE.getUsage());
				return;
			}
			String arena = args[1];
			if (am.getArena(arena) == null) {
				sender.sendMessage("" + ChatColor.RED + "Invalid arena name.");
				return;
			}
			String id = arena.toLowerCase();
			am.deleteArena(id);
			sender.sendMessage(
					"" + ChatColor.GREEN + "Successfully deleted the arena '" + id + "'!");
		}
	}

}
