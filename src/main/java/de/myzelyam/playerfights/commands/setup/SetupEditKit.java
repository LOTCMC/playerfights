package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.DataMgr;
import de.myzelyam.playerfights.commands.Command;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SetupEditKit extends Command implements Listener {

    private static Map<UUID, String> uuidKitMap = new HashMap<>();

    /* true = inventory
     false = armor */
    private static Map<UUID, DataMgr.SlotType> uuidSlotTypeMap = new HashMap<>();

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (canDo(sender, CommandType.SETUP_EDITKIT)) {
            Player p = (Player) sender;
            if (args.length != 3) {
                sender.sendMessage(ChatColor.RED + "Invalid usage! "
                        + CommandType.SETUP_EDITKIT.getUsage());
                return;
            }
            DataMgr.SlotType slotType = DataMgr.SlotType.fromString(args[1]);
            String kit = args[2];
            if (!plugin.getDataMgr().getKits().contains(kit.toLowerCase())) {
                sender.sendMessage(ChatColor.RED + "Invalid kit name.");
                return;
            }
            if (slotType == null) {
                sender.sendMessage(ChatColor.RED + "Invalid usage! "
                        + CommandType.SETUP_EDITKIT.getUsage());
                return;
            }
            uuidKitMap.put(p.getUniqueId(), kit.toLowerCase());
            uuidSlotTypeMap.put(p.getUniqueId(), slotType);
            Inventory inv = Bukkit.createInventory(null, slotType == DataMgr.SlotType.STORAGE ? 36 : 9, "Kit " +
                    (slotType == DataMgr.SlotType.STORAGE ? "inventory" : slotType == DataMgr.SlotType.ARMOR ?
                            "armor" : "off-hand") + " editor of " + kit);
            ItemStack[] items = plugin.getDataMgr().getKitSlots(kit, slotType);
            inv.setContents(items);
            if (slotType == DataMgr.SlotType.ARMOR) {
                for (int i = 4; i < 9; i++)
                    inv.setItem(i, new ItemStack(Material.STAINED_GLASS_PANE));
            } else if (slotType == DataMgr.SlotType.EXTRA) {
                for (int i = 1; i < 9; i++)
                    inv.setItem(i, new ItemStack(Material.STAINED_GLASS_PANE));
            }
            p.openInventory(inv);
            p.sendMessage(ChatColor.YELLOW + "Now editing the " +
                    (slotType == DataMgr.SlotType.STORAGE ? "inventory" : slotType == DataMgr.SlotType.ARMOR ?
                            "armor" : "off-hand") + " of " + kit + "!");
        }
    }


    @EventHandler(priority = EventPriority.HIGH)
    public void onClick(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player)) return;
        Player p = (Player) e.getWhoClicked();
        String kit = uuidKitMap.get(p.getUniqueId());
        if (kit == null) return;
        DataMgr.SlotType slotType = uuidSlotTypeMap.get(p.getUniqueId());
        if (slotType != DataMgr.SlotType.STORAGE &&
                e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.STAINED_GLASS_PANE) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onInventoryClose(InventoryCloseEvent e) {
        if (!(e.getPlayer() instanceof Player)) return;
        Player p = (Player) e.getPlayer();
        String kit = uuidKitMap.get(p.getUniqueId());
        if (kit == null) return;
        DataMgr.SlotType slotType = uuidSlotTypeMap.get(p.getUniqueId());
        plugin.getDataMgr().setKitSlots(kit, e.getInventory().getContents(), slotType);
        uuidKitMap.remove(p.getUniqueId());
        uuidSlotTypeMap.remove(p.getUniqueId());
        p.sendMessage(ChatColor.GREEN + "Saved " + kit + "!");
    }
}