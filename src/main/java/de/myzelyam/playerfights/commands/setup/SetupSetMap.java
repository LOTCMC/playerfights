package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.Arena;
import de.myzelyam.playerfights.ArenaState;
import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class SetupSetMap extends Command {

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (canDo(sender, CommandType.SETUP_SETMAP)) {
			if (args.length != 3) {
				sender.sendMessage("" + ChatColor.RED + "Invalid usage! "
						+ CommandType.SETUP_SETMAP.getUsage());
				return;
			}
			String arena = args[1];
			String map = args[2];
			if (am.getArena(arena) == null) {
				sender.sendMessage("" + ChatColor.RED + "Invalid arena name.");
				return;
			}
			if (!plugin.getDataMgr().getMapIDs().contains(map.toLowerCase())) {
				sender.sendMessage("" + ChatColor.RED + "Invalid map. Use /pfsetup addmap <map>");
				return;
			}
			Arena a = am.getArena(arena);
			if (a.getState() != ArenaState.DISABLED) {
				sender.sendMessage(
						"" + ChatColor.RED + "You have to disable the arena first to do this! '/pfsetup toggle "
								+ a.getDisplayId() + "'");
				return;
			}
			a.setMap(map.toLowerCase());
			plugin.getDataMgr().setMapOfArena(a.getId(), map.toLowerCase());
			sender.sendMessage("" + ChatColor.GREEN + "Successfully set the map of the arena '"
					+ a.getDisplayId() + "' to '" + map.toLowerCase() + "'!");
		}
	}

}
