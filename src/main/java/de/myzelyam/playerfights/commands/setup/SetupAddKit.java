package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetupAddKit extends Command {

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (canDo(sender, CommandType.SETUP_ADDKIT)) {
            Player p = (Player) sender;
            if (args.length != 2) {
                sender.sendMessage(ChatColor.RED + "Invalid usage! "
                        + CommandType.SETUP_ADDKIT.getUsage());
                return;
            }
            String kit = args[1];
            if (plugin.getDataMgr().getKits().contains(kit.toLowerCase())) {
                sender.sendMessage(ChatColor.RED + "Invalid kit name.");
                return;
            }
            plugin.getDataMgr().addKit(kit, p.getInventory());
            p.sendMessage(ChatColor.GREEN + "Successfully added a kit with the name '" + kit
                    + "' using the items in your inventory as equipment!");
        }
    }

}
