package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.Arena;
import de.myzelyam.playerfights.ArenaState;
import de.myzelyam.playerfights.ArenaStopReason;
import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class SetupToggle extends Command {

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (canDo(sender, CommandType.SETUP_TOGGLE)) {
            if (args.length != 2) {
                sender.sendMessage("" + ChatColor.RED + "Invalid usage! "
                        + CommandType.SETUP_TOGGLE.getUsage());
                return;
            }
            String arenaName = args[1];
            Arena a = am.getArena(arenaName);
            if (a == null) {
                sender.sendMessage("" + ChatColor.RED + "Invalid arena name.");
                return;
            }
            boolean isReady = a.isReadyForUse();
            if (!isReady) {
                sender.sendMessage(
                        "" + ChatColor.RED + "This arena is not ready for use! Use '/pfsetup check "
                                + a.getDisplayId() + "' for more information!");
                return;
            }
            boolean enabled = a.getState() != ArenaState.DISABLED;
            if (enabled) {
                a.forceEnd(ArenaStopReason.FORCED_STOP);
                a.setState(ArenaState.DISABLED);
            } else {
                a.setState(ArenaState.WAITING);
            }
            sender.sendMessage(
                    "" + ChatColor.GOLD + "Successfully " + (enabled ? "" + ChatColor.RED + "disabled" : "" + ChatColor.GREEN + "enabled")
                            + " " + ChatColor.GOLD + "the arena '" + ChatColor.YELLOW + "" + a.getDisplayId() + "" + ChatColor.GOLD + "'!");
        }
    }
}
