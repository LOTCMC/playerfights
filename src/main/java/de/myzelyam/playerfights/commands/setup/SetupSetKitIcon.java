package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetupSetKitIcon extends Command {

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (canDo(sender, CommandType.SETUP_SETKITICON)) {
			Player p = (Player) sender;
			if (args.length != 2) {
				sender.sendMessage("" + ChatColor.RED + "Invalid usage! "
						+ CommandType.SETUP_SETKITICON.getUsage());
				return;
			}
			String kit = args[1].toLowerCase();
			if (!plugin.getDataMgr().getKits().contains(kit)) {
				sender.sendMessage("" + ChatColor.RED + "Invalid kit name.");
				return;
			}
			plugin.getDataMgr().setIconOfKit(kit, p.getItemInHand());
			p.sendMessage("" + ChatColor.GREEN + "Successfully set the icon for the kit '" + kit
					+ "' to the  item in your hand!");
		}
	}

}
