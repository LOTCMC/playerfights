package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class SetupReload extends Command {

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (canDo(sender, CommandType.SETUP_RELOAD)) {
			plugin.getDataFile().reload();
			plugin.setData(plugin.getDataFile().getConfig());
			sender.sendMessage("" + ChatColor.GREEN + "Sucessfully reloaded the data.yml file.");
		}
	}
}