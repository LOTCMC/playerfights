package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class SetupRemoveKit extends Command {

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (canDo(sender, CommandType.SETUP_REMOVEKIT)) {
			if (args.length != 2) {
				sender.sendMessage("" + ChatColor.RED + "Invalid usage! "
						+ CommandType.SETUP_REMOVEKIT.getUsage());
				return;
			}
			String kit = args[1].toLowerCase();
			if (!plugin.getDataMgr().getKits().contains(kit)) {
				sender.sendMessage("" + ChatColor.RED + "Invalid kit name.");
				return;
			}
			plugin.getDataMgr().removeKit(kit);
			sender.sendMessage("" + ChatColor.GREEN + "Successfully removed the kit '" + kit + "'!");
		}
	}

}
