package de.myzelyam.playerfights.commands.setup;

import de.myzelyam.playerfights.ArenaType;
import de.myzelyam.playerfights.commands.Command;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class SetupCreate extends Command {

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (canDo(sender, CommandType.SETUP_CREATE)) {
			if (args.length != 3) {
				sender.sendMessage("" + ChatColor.RED + "Invalid usage! "
						+ CommandType.SETUP_CREATE.getUsage());
				return;
			}
			String arena = args[1];
			String typeString = args[2];
			if (am.getArena(arena) != null || arena.length() > 16) {
				sender.sendMessage("" + ChatColor.RED + "Invalid arena name.");
				return;
			}
			ArenaType type = ArenaType.fromString(typeString);
			if (type == null) {
				sender.sendMessage("" + ChatColor.RED + "Invalid type.");
				return;
			}
			String id = arena.toLowerCase();
			am.createArena(id, arena, type, null, null);
			sender.sendMessage("" + ChatColor.GREEN + "Successfully created an arena with the id '"
					+ id + "' and the displayname '" + arena
					+ "'! You can use '/pfsetup check " + id
					+ "' to see its state.");
		}
	}

}
