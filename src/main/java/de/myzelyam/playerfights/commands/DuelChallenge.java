package de.myzelyam.playerfights.commands;

import de.myzelyam.playerfights.ArenaType;
import de.myzelyam.playerfights.PVHook;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DuelChallenge extends Command {

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (canDo(sender, CommandType.DUEL_CHALLENGE)) {
            Player p = (Player) sender;
            if (am.isInGame(p)
                    || plugin.getChallengeMgr().preTeleportPlayers.contains(p)) {
                p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                        + ChatColor.RED + "You are already in an arena!");
                return;
            }
            if (args.length != 1 && args.length != 2) {
                p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                        + ChatColor.RED +
                        "Invalid usage! /duel <<Player>|accept|deny|block|unblock> [Player|amountOfFights]");
                return;
            }
            if (Bukkit.getPlayer(args[0]) != null) {
                Player target = Bukkit.getPlayer(args[0]);

                int amount = 1;
                if (args.length == 2)
                    args[1] = args[1].toLowerCase().replace("best-of-", "");
                try {
                    if (args.length == 2)
                        amount = Integer.parseInt(args[1]);
                    if (amount != 1 && amount != 3 && amount != 5
                            && amount != 9) {
                        throw new NumberFormatException();
                    }
                } catch (NumberFormatException e) {
                    p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                            + ChatColor.RED + "Invalid amount of fights, allowed amounts:\n" + ChatColor.GOLD +
                            "Best-of-1" + ChatColor.RED + "," + ChatColor.GOLD + " Best-of-3" + ChatColor.RED +
                            "," + ChatColor.GOLD + " Best-of-5" + ChatColor.RED + "," + ChatColor.GOLD + " Best-of-9");
                    return;
                }
                if (target == null || (Bukkit.getPluginManager()
                        .isPluginEnabled("PremiumVanish")
                        && PVHook.isVanished(target))) {
                    p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                            + ChatColor.RED + "This player isn't online right now!");
                    return;
                }
                if (Bukkit.getPluginManager().isPluginEnabled("PremiumVanish")
                        && PVHook.isVanished(p)) {
                    p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                            + ChatColor.RED + "Please go out of vanish first.");
                    return;
                }
                if (p.getUniqueId().toString()
                        .equals(target.getUniqueId().toString())) {
                    p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                            + ChatColor.RED + "You can't challenge yourself.");
                    return;
                }
                if (am.isInGame(target) || plugin.getChallengeMgr().preTeleportPlayers
                        .contains(target)) {
                    p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                            + ChatColor.RED + "This player is already fighting against someone else right now!");
                    return;
                }
                if (plugin.getChallengeMgr().isBlocked(p, target,
                        ArenaType.DUEL)) {
                    p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                            + ChatColor.RED + "This player doesn't allow you to challenge him anymore.");
                    return;
                }
                if (!isWorldAllowed(p, target)) {
                    p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                            + ChatColor.RED +
                            "Either you or the player you want to duel against is in a blacklisted world.");
                    return;
                }
                sendChallenge(p, target, amount);
            } else if (args.length == 2) {
                String action = args[0];
                String fromString = args[1];
                Player from = Bukkit.getPlayer(fromString);
                if (from == null) {
                    p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                            + ChatColor.RED + "This player isn't online!");
                    return;
                }
                if (action.equalsIgnoreCase("accept")) {
                    if (!plugin.getChallengeMgr().hasPendingChallenge(p)) {
                        p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                                + ChatColor.RED + "You don't have any pending challenge currently!");
                        return;
                    }
                    plugin.getChallengeMgr().acceptChallenge(p, from);
                } else if (action.equalsIgnoreCase("deny")) {
                    if (!plugin.getChallengeMgr().hasPendingChallenge(p)) {
                        p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                                + ChatColor.RED + "You don't have any pending challenge currently!");
                        return;
                    }
                    plugin.getChallengeMgr().denyChallenge(p, from, false);
                } else if (action.equalsIgnoreCase("block")) {
                    if (!plugin.getChallengeMgr().hasPendingChallenge(p)) {
                        p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                                + ChatColor.RED + "You don't have any pending challenge currently!");
                        return;
                    }
                    plugin.getChallengeMgr().blockChallenge(from, p,
                            ArenaType.DUEL);
                } else if (action.equalsIgnoreCase("unblock")) {
                    if (plugin.getChallengeMgr().unblockChallenge(from, p,
                            ArenaType.DUEL))
                        p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                                + ChatColor.YELLOW + "Successfully unblocked challenges from " + ChatColor.GOLD
                                + from.getName() + ChatColor.YELLOW + "!");
                    else
                        p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                                + ChatColor.RED + "You weren't blocking challenges from that player!");

                } else {
                    p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                            + ChatColor.RED +
                            "Invalid usage! /duel <<Player>|accept|deny|block|unblock> [Player|amountOfFights]");
                }
            } else {
                p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                        + ChatColor.RED +
                        "Invalid usage! /duel <<Player>|accept|deny|block|unblock> [Player|amountOfFights]");
            }
        }
    }

    private void sendChallenge(Player p, Player target, int amountOfFights) {
        if (plugin.getChallengeMgr().addChallenge(target, p, ArenaType.DUEL,
                amountOfFights)) {
            p.sendMessage(plugin.getPrefix(ArenaType.DUEL)
                    + "" + ChatColor.YELLOW + "Successfully sent a challenge to " + target.getName()
                    + "" + ChatColor.YELLOW + ", please wait for that player to accept the challenge.");
            ComponentBuilder msg = new ComponentBuilder("Click one:  ")
                    .color(ChatColor.GRAY);
            //
            HoverEvent acceptHover = new HoverEvent(Action.SHOW_TEXT,
                    new ComponentBuilder("Click to accept the challenge!")
                            .create());
            HoverEvent denyHover = new HoverEvent(Action.SHOW_TEXT,
                    new ComponentBuilder("Click to deny the challenge!")
                            .create());
            HoverEvent blockHover = new HoverEvent(Action.SHOW_TEXT,
                    new ComponentBuilder(
                            "Click to deny the challenge\nand block prospective challenges\nfrom "
                                    + p.getName() + "!").create());
            //
            ClickEvent acceptClick = new ClickEvent(
                    net.md_5.bungee.api.chat.ClickEvent.Action.RUN_COMMAND,
                    "/duel accept " + p.getName());
            ClickEvent denyClick = new ClickEvent(
                    net.md_5.bungee.api.chat.ClickEvent.Action.RUN_COMMAND,
                    "/duel deny " + p.getName());
            ClickEvent blockClick = new ClickEvent(
                    net.md_5.bungee.api.chat.ClickEvent.Action.RUN_COMMAND,
                    "/duel block " + p.getName());
            //
            msg = msg.append("   ACCEPT").color(ChatColor.GREEN).bold(true)
                    .event(acceptHover).event(acceptClick).append("  ")
                    .append("-").color(ChatColor.WHITE).append("   DENY")
                    .color(ChatColor.RED).bold(true).event(denyHover)
                    .event(denyClick).append("  ").append("-")
                    .color(ChatColor.WHITE).append("   BLOCK   ")
                    .color(ChatColor.GRAY).bold(true).event(blockHover)
                    .event(blockClick);
            target.sendMessage(
                    "§8---------------------------------------------");
            target.sendMessage("" + ChatColor.RED + "" + p.getName()
                    + "" + ChatColor.GOLD + " would like to fight against you in a Best-of-"
                    + numberToString(amountOfFights) + " duel!");
            target.spigot().sendMessage(msg.create());
            target.sendMessage(
                    "§8---------------------------------------------");
        }
    }

    private boolean isWorldAllowed(Player to, Player from) {
        boolean worldToOkay = false, worldFromOkay = false;
        for (String world : plugin.getConfig()
                .getStringList("WhitelistedJoinWorlds")) {
            if (to.getWorld().getName().equalsIgnoreCase(world)) {
                worldToOkay = true;
            }
            if (from.getWorld().getName().equalsIgnoreCase(world)) {
                worldFromOkay = true;
            }
        }
        return worldToOkay && worldFromOkay;
    }

    private String numberToString(int number2) {
        String number = "";
        switch (number2) {
            case 1:
                number = "One";
                break;
            case 2:
                number = "Two";
                break;
            case 3:
                number = "Three";
                break;
            case 4:
                number = "Four";
                break;
            case 5:
                number = "Five";
                break;
            case 9:
                number = "Nine";
                break;
        }
        return number;
    }
}
