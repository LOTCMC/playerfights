package de.myzelyam.playerfights;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KitMgr implements Listener {

    // two minutes
    public static final int TIMEOUT = 60 * 2;

    private final Arena arena;

    protected BukkitTask timeoutTask, reopenTask;

    protected int reopenTaskTime = 0;

    public Map<Player, String> selectedKit = new HashMap<>();

    public KitMgr(Arena arena) {
        this.arena = arena;
        arena.getPlugin().getServer().getPluginManager().registerEvents(this,
                arena.getPlugin());
    }

    public void start() {
        for (Player p : arena) {
            selectedKit.put(p, null);
            openKitGUI(p);
        }
        timeoutTask = new BukkitRunnable() {

            @Override
            public void run() {
                reopenTaskTime++;
                reopenInventoriesIfRequired();
                if (reopenTaskTime >= TIMEOUT) {
                    stop();
                    arena.sendArenaMessageWithPrefix(ChatColor.RED + "One player failed to select his kit, stopping arena.");
                    arena.forceEnd(ArenaStopReason.FORCED_STOP);
                    cancel();
                }
            }
        }.runTaskTimer(arena.getPlugin(), 20, 20);
    }

    public void reopenInventoriesIfRequired() {
        for (Player p : arena) {
            if (selectedKit.get(p) == null)
                openKitGUI(p);
        }
    }

    protected void openKitGUI(Player p) {
        List<String> kits = arena.getPlugin().getDataMgr().getKits();
        Inventory inv = Bukkit.createInventory(null, kits.size() > 9 ? 18 : 9,
                "Choose your kit!");
        for (String kitId : kits) {
            ItemStack item = arena.getPlugin().getDataMgr().getIconOfKit(kitId);
            if (item == null)
                item = new ItemStack(Material.IRON_SWORD);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.GOLD + "" + arena.getPlugin().getDataMgr().getKitDisplayName(kitId));
            item.setItemMeta(meta);
            inv.addItem(item);
        }
        p.openInventory(inv);
    }

    private void sendPlayerSelectedKitMsg(Player p, String kitId, boolean reveal) {
        boolean alwaysRevealAnyway = arena.getArenaMgr().getPlugin().getConfig().getBoolean("SpoilKit");
        if (alwaysRevealAnyway && reveal) return;
        if (reveal) {
            arena.sendArenaMessageWithPrefix(p.getDisplayName()
                    + ChatColor.GOLD + " has chosen the kit " + ChatColor.RED
                    + arena.getPlugin().getDataMgr().getKitDisplayName(kitId) + ChatColor.GOLD + "!");
        } else if (!alwaysRevealAnyway) {
            arena.sendArenaMessageWithPrefix(p.getDisplayName()
                    + ChatColor.GRAY + " selected a kit!");
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerSelectKit(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player))
            return;
        Player p = (Player) e.getWhoClicked();
        if (arena != arena.getArenaMgr().getArena(p))
            return;
        // valid?
        if (e.getCurrentItem() == null
                || e.getCurrentItem().getItemMeta() == null
                || e.getCurrentItem().getItemMeta().getDisplayName() == null)
            return;
        if (selectedKit.get(p) != null)
            return;
        e.setCancelled(true);
        String kitId = e.getCurrentItem().getItemMeta().getDisplayName()
                .replace("" + ChatColor.GOLD, "").toLowerCase();
        if (!arena.getPlugin().getDataMgr().getKits().contains(kitId))
            return;
        // give kit
        arena.getPlugin().getDataMgr().applyKit(p.getInventory(), kitId);
        p.updateInventory();
        sendPlayerSelectedKitMsg(p, kitId, false);
        // mark as selected
        selectedKit.put(p, kitId);
        p.closeInventory();
        // did all players select their kit? If so start the duel!
        for (Player p2 : selectedKit.keySet()) {
            if (selectedKit.get(p2) == null) {
                // definitly not
                return;
            }
        }
        stop();
        // send the message after everyone has selected their kit
        for (Player player : arena.getPlayers())
            sendPlayerSelectedKitMsg(player, selectedKit.get(player), true);
        arena.startCountdown();
    }

    public void giveKitBack(Player p) {
        if (selectedKit.get(p) != null) {
            arena.getPlugin().getDataMgr().applyKit(p.getInventory(), selectedKit.get(p));
            p.updateInventory();
        }
    }

    public void stop() {
        if (timeoutTask != null)
            timeoutTask.cancel();
        reopenTaskTime = 0;
        for (Player p : arena)
            p.closeInventory();
        // keep kits for now
    }

    public void reset() {
        stop();
        selectedKit.clear();
        reopenTaskTime = 0;
    }
}
