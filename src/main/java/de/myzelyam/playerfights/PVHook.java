package de.myzelyam.playerfights;

import org.bukkit.entity.Player;

import de.myzelyam.api.vanish.VanishAPI;

public abstract class PVHook {

	public static boolean isVanished(Player p) {
		return VanishAPI.isInvisible(p);
	}
}
