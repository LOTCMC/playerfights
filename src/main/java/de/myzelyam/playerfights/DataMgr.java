package de.myzelyam.playerfights;

import com.sun.istack.internal.Nullable;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class DataMgr {

    private final PlayerFights plugin;

    private FileConfiguration getData() {
        return plugin.getData();
    }

    public DataMgr(PlayerFights plugin) {
        this.plugin = plugin;
    }

    public List<String> getArenaIDs() {
        return getData().getStringList("Arenas.ArenaIDs");
    }

    public boolean addArenaIDToList(String id) {
        // validate given id
        id = id.toLowerCase();
        // add id
        List<String> IDList = getArenaIDs();
        if (!IDList.add(id))
            return false;
        getData().set("Arenas.ArenaIDs", IDList);
        plugin.getDataFile().save();
        return true;
    }

    public boolean removeArenaIDFromList(String id) {
        // validate given id
        id = id.toLowerCase();
        // remove id
        List<String> IDList = getArenaIDs();
        if (!IDList.remove(id))
            return false;
        getData().set("Arenas.ArenaIDs", IDList);
        plugin.getDataFile().save();
        return true;
    }

    public void createArenaSection(String id, String displayId,
                                   ArenaType type) {
        id = id.toLowerCase();
        getData().set("Arenas." + id + ".displayId", displayId);
        getData().set("Arenas." + id + ".type", type.toString());
        plugin.getDataFile().save();
    }

    public void deleteArenaSection(String id) {
        id = id.toLowerCase();
        getData().set("Arenas." + id, null);
        plugin.getDataFile().save();
    }

    public ArenaType getArenaType(String id) {
        return ArenaType
                .fromString(getData().getString("Arenas." + id + ".type"));
    }

    public void addSpawnLocForArena(String id, Location spawnLoc) {
        List<Location> newLocs = getSpawnLocationsOfArena(id);
        newLocs.add(spawnLoc);
        setSpawnLocsForArena(id, newLocs);
        plugin.getDataFile().save();
    }

    public void setSpawnLocsForArena(String id, List<Location> spawnLocs) {
        id = id.toLowerCase();
        if (spawnLocs == null)
            return;
        getData().set("Arenas." + id + ".spawnLocs.amount", spawnLocs.size());
        for (int i = 0; i < spawnLocs.size(); i++) {
            getData().set("Arenas." + id + ".spawnLocs.n" + i,
                    serializeLoc(spawnLocs.get(i)));
        }
        plugin.getDataFile().save();
    }

    public List<Location> getSpawnLocationsOfArena(String id) {
        List<Location> locations = new LinkedList<>();
        for (int i = 0; i < getData()
                .getInt("Arenas." + id + ".spawnLocs.amount"); i++) {
            Location loc = deserializeLoc(
                    getData().getString("Arenas." + id + ".spawnLocs.n" + i));
            locations.add(loc);
        }
        return locations;
    }

    public String serializeLoc(Location locObj) {
        double x = locObj.getX(), y = locObj.getY(), z = locObj.getZ(),
                ya = locObj.getYaw(), pi = locObj.getPitch();
        return locObj.getWorld().getName() + ":" + x + ":" + y + ":" + z + ":"
                + ya + ":" + pi;
    }

    public Location deserializeLoc(String locStr) {
        String[] values = locStr.split(":");
        double x = Double.parseDouble(values[1]),
                y = Double.parseDouble(values[2]),
                z = Double.parseDouble(values[3]),
                ya = Double.parseDouble(values[4]),
                pi = Double.parseDouble(values[5]);
        Location loc = new Location(Bukkit.getWorld(values[0]), x, y, z);
        loc.setYaw((float) ya);
        loc.setPitch((float) pi);
        return loc;
    }

    public void setMapOfArena(String id, String map) {
        if (map == null)
            return;
        getData().set("Arenas." + id + ".map", map);
        plugin.getDataFile().save();
    }

    public String getMapOfArena(String id) {
        return getData().getString("Arenas." + id + ".map");
    }

    public void createMap(String map, @Nullable ItemStack icon) {
        getData().set("Maps." + map.toLowerCase() + ".displayId", map);
        if (icon != null)
            getData().set("Maps." + map.toLowerCase() + ".icon", icon);
        List<String> maps = getData().getStringList("MapList");
        maps.add(map.toLowerCase());
        getData().set("MapList", maps);
        plugin.getDataFile().save();
    }

    public void deleteMap(String map) {
        // delete whole section
        getData().set("Maps." + map.toLowerCase(), null);
        // remove from map list
        List<String> maps = getData().getStringList("MapList");
        maps.remove(map.toLowerCase());
        getData().set("MapList", maps);
        plugin.getDataFile().save();
    }

    public ItemStack getIconOfMap(String map) {
        return getData().getItemStack("Maps." + map.toLowerCase() + ".icon");
    }

    public List<String> getMapIDs() {
        return getData().getStringList("MapList");
    }

    public void addKit(String kit, PlayerInventory inv) {
        getData().set("Kits." + kit.toLowerCase() + ".displayName", kit);
        for (int i = 0; i < inv.getContents().length; i++)
            getData().set("Kits." + kit.toLowerCase() + ".invContents." + i,
                    inv.getContents()[i]);
        for (int i = 0; i < inv.getArmorContents().length; i++)
            getData().set("Kits." + kit.toLowerCase() + ".armorContents." + i,
                    inv.getArmorContents()[i]);
        List<String> maps = getData().getStringList("KitList");
        maps.add(kit.toLowerCase());
        getData().set("KitList", maps);
        plugin.getDataFile().save();
    }

    /**
     * Warning: Overrides inventory -> Save inventory first
     */
    public void applyKit(PlayerInventory inv, String id) {
        ItemStack[] invContents = new ItemStack[36];
        for (int i = 0; i < 36; i++)
            invContents[i] = getData().getItemStack(
                    "Kits." + id.toLowerCase() + ".invContents." + i,
                    new ItemStack(Material.AIR));
        ItemStack[] armorContents = new ItemStack[4];
        for (int i = 0; i < 4; i++)
            armorContents[i] = getData().getItemStack(
                    "Kits." + id.toLowerCase() + ".armorContents." + i,
                    new ItemStack(Material.AIR));
        ItemStack offHand = getData().getItemStack(
                "Kits." + id.toLowerCase() + ".extraContents.0",
                new ItemStack(Material.AIR));
        inv.setContents(invContents);
        inv.setArmorContents(armorContents);
        inv.setItemInOffHand(offHand);
    }

    public ItemStack[] getKitSlots(String id, SlotType type) {
        if (type == SlotType.STORAGE) {
            ItemStack[] invContents = new ItemStack[36];
            for (int i = 0; i < 36; i++)
                invContents[i] = getData().getItemStack(
                        "Kits." + id.toLowerCase() + ".invContents." + i,
                        new ItemStack(Material.AIR));
            return invContents;
        } else if (type == SlotType.ARMOR) {
            ItemStack[] armorContents = new ItemStack[4];
            for (int i = 0; i < 4; i++)
                armorContents[i] = getData().getItemStack(
                        "Kits." + id.toLowerCase() + ".armorContents." + i,
                        new ItemStack(Material.AIR));
            return armorContents;
        } else {
            ItemStack[] extraContents = new ItemStack[1];
            for (int i = 0; i < 1; i++)
                extraContents[i] = getData().getItemStack(
                        "Kits." + id.toLowerCase() + ".extraContents." + i,
                        new ItemStack(Material.AIR));
            return extraContents;
        }
    }

    public void setKitSlots(String kit, ItemStack[] itemStacks, SlotType slotType) {
        if (slotType == SlotType.STORAGE)
            for (int i = 0; i < 36; i++)
                getData().set("Kits." + kit.toLowerCase() + ".invContents." + i,
                        itemStacks[i]);
        else if (slotType == SlotType.ARMOR)
            for (int i = 0; i < 4; i++)
                getData().set("Kits." + kit.toLowerCase() + ".armorContents." + i,
                        itemStacks[i]);
        else
            for (int i = 0; i < 1; i++)
                getData().set("Kits." + kit.toLowerCase() + ".extraContents." + i,
                        itemStacks[i]);
        plugin.getDataFile().save();
    }


    public void removeKit(String id) {
        // section
        getData().set("Kits." + id.toLowerCase(), null);
        // list
        List<String> maps = getData().getStringList("KitList");
        maps.remove(id.toLowerCase());
        getData().set("KitList", maps);
        plugin.getDataFile().save();
    }

    public void setIconOfKit(String id, ItemStack icon) {
        getData().set("Kits." + id.toLowerCase() + ".icon", icon);
        plugin.getDataFile().save();
    }

    public ItemStack getIconOfKit(String id) {
        return getData().getItemStack("Kits." + id.toLowerCase() + ".icon");
    }

    public String getKitDisplayName(String id) {
        return getData().getString("Kits." + id.toLowerCase() + ".displayName");
    }

    public List<String> getKits() {
        return getData().getStringList("KitList");
    }

    public String getArenaDisplayId(String id) {
        return getData().getString("Arenas." + id + ".displayId");
    }

    public void addWinToPlayer(UUID uuid, ArenaType type) {
        getData().set(
                "PlayerData." + uuid.toString() + "." + type.toString()
                        + ".wins",
                getData().getInt("PlayerData." + uuid.toString() + "."
                        + type.toString() + ".wins") + 1);
        plugin.getDataFile().save();
    }

    public void addDefeatToPlayer(UUID uuid, ArenaType type) {
        getData().set(
                "PlayerData." + uuid.toString() + "." + type.toString()
                        + ".defeats",
                getData().getInt("PlayerData." + uuid.toString() + "."
                        + type.toString() + ".defeats") + 1);
        plugin.getDataFile().save();
    }

    public int getWins(UUID uuid, ArenaType type) {
        return getData().getInt("PlayerData." + uuid.toString() + "."
                + type.toString() + ".wins");
    }

    public int getDefeats(UUID uuid, ArenaType type) {
        return getData().getInt("PlayerData." + uuid.toString() + "."
                + type.toString() + ".defeats");
    }

    private List<String> getBlockedPlayerUUIDStrings(UUID blockedFrom,
                                                     ArenaType type) {
        return getData().getStringList("PlayerData." + blockedFrom.toString()
                + "." + type.toString() + ".blockedPlayers");
    }

    public void blockPlayer(UUID blocked, UUID from, ArenaType type) {
        List<String> list = getBlockedPlayerUUIDStrings(from, type);
        list.add(blocked.toString());
        getData().set("PlayerData." + from.toString() + "." + type.toString()
                + ".blockedPlayers", list);
        plugin.getDataFile().save();
    }

    public void unblockPlayer(UUID blocked, UUID from, ArenaType type) {
        List<String> list = getBlockedPlayerUUIDStrings(from, type);
        list.remove(blocked.toString());
        getData().set("PlayerData." + from.toString() + "." + type.toString()
                + ".blockedPlayers", list);
        plugin.getDataFile().save();
    }

    public boolean isBlocked(UUID blocked, UUID from, ArenaType type) {
        return getBlockedPlayerUUIDStrings(from, type)
                .contains(blocked.toString());
    }

    public enum SlotType {
        STORAGE, ARMOR, EXTRA;

        public static SlotType fromString(String s) {
            if (s.equalsIgnoreCase("inv") || s.equalsIgnoreCase("storage")) return SlotType.STORAGE;
            if (s.equalsIgnoreCase("armor")) return SlotType.ARMOR;
            if (s.equalsIgnoreCase("extra") || s.equalsIgnoreCase("offhand") || s.equalsIgnoreCase("off-hand"))
                return SlotType.EXTRA;
            return null;
        }
    }
}
