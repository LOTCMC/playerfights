package de.myzelyam.playerfights;

import de.myzelyam.playerfights.commands.CommandMgr;
import de.myzelyam.playerfights.commands.setup.SetupEditKit;
import de.myzelyam.playerfights.files.FileMgr;
import de.myzelyam.playerfights.files.FileMgr.FileType;
import de.myzelyam.playerfights.files.PluginFile;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class PlayerFights extends JavaPlugin {

    private static PlayerFights instance;

    public static PlayerFights getInstance() {
        return instance;
    }

    private FileMgr fileMgr;

    private ArenaMgr arenaMgr;

    private ArenaLoader arenaLoader;

    private DataMgr dataMgr;

    private ConfigMgr configMgr;

    private CommandMgr commandMgr;

    private PlayerDataMgr inventoryMgr;

    private ChallengeMgr challengeMgr;

    private PluginFile<FileConfiguration> dataFile, configFile;

    private FileConfiguration data, config;

    public DamageActionBarMgr damageActionBarMgr;

    @SuppressWarnings("unchecked")
    @Override
    public void onEnable() {
        // initialize managers and files
        setInstance(this);
        setFileMgr(new FileMgr());
        dataFile = (PluginFile<FileConfiguration>) getFileMgr().addFile("data",
                FileType.BUKKIT_STORAGE);
        configFile = (PluginFile<FileConfiguration>) getFileMgr()
                .addFile("config", FileType.BUKKIT_CONFIG);
        setData(dataFile.getConfig());
        setConfig(configFile.getConfig());
        setDataMgr(new DataMgr(this));
        setArenaMgr(new ArenaMgr(this));
        setArenaLoader(new ArenaLoader(this));
        setInventoryMgr(new PlayerDataMgr(this));
        challengeMgr = new ChallengeMgr(this);
        commandMgr = new CommandMgr(this);
        // load arenas
        getArenaLoader().loadArenas();
        // register events
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new GeneralPlayerListener(this), this);
        pm.registerEvents(new SetupEditKit(), this);
        // action bars
        this.damageActionBarMgr = new DamageActionBarMgr();
        // anti sneak
        AntiSneakV2.registerListener(this);
        // aac hook
        if (pm.getPlugin("AAC") != null)
            new AACHook(this);
    }

    @Override
    public void onDisable() {
        for (Arena a : getArenaMgr().getAllArenas())
            a.forceEnd(ArenaStopReason.FORCED_STOP);
        setInstance(null);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command,
                             String label, String[] args) {
        commandMgr.execute(command, sender, args);
        return true;
    }

    public String getPrefix(ArenaType type) {
        return ChatColor.BLUE + "[" + type.makeNice() + "] " + ChatColor.YELLOW + "";
    }

    public String getPrefix() {
        return ChatColor.BLUE + "[PlayerFights]" + ChatColor.YELLOW + " ";
    }

    // getters & setters

    public FileMgr getFileMgr() {
        return fileMgr;
    }

    public void setFileMgr(FileMgr fileMgr) {
        this.fileMgr = fileMgr;
    }

    public ArenaMgr getArenaMgr() {
        return arenaMgr;
    }

    public void setArenaMgr(ArenaMgr arenaMgr) {
        this.arenaMgr = arenaMgr;
    }

    public ArenaLoader getArenaLoader() {
        return arenaLoader;
    }

    public void setArenaLoader(ArenaLoader arenaLoader) {
        this.arenaLoader = arenaLoader;
    }

    public DataMgr getDataMgr() {
        return dataMgr;
    }

    public void setDataMgr(DataMgr dataMgr) {
        this.dataMgr = dataMgr;
    }

    public ConfigMgr getConfigMgr() {
        return configMgr;
    }

    public void setConfigMgr(ConfigMgr configMgr) {
        this.configMgr = configMgr;
    }

    public CommandMgr getCommandMgr() {
        return commandMgr;
    }

    public void setCommandMgr(CommandMgr commandMgr) {
        this.commandMgr = commandMgr;
    }

    public PlayerDataMgr getInventoryMgr() {
        return inventoryMgr;
    }

    public void setInventoryMgr(PlayerDataMgr inventoryMgr) {
        this.inventoryMgr = inventoryMgr;
    }

    @Override
    public FileConfiguration getConfig() {
        return config;
    }

    public void setConfig(FileConfiguration config) {
        this.config = config;
    }

    public FileConfiguration getData() {
        return data;
    }

    public void setData(FileConfiguration data) {
        this.data = data;
    }

    public PluginFile<FileConfiguration> getDataFile() {
        return dataFile;
    }

    public void setDataFile(PluginFile<FileConfiguration> dataFile) {
        this.dataFile = dataFile;
    }

    public PluginFile<FileConfiguration> getConfigFile() {
        return configFile;
    }

    public void setConfigFile(PluginFile<FileConfiguration> configFile) {
        this.configFile = configFile;
    }

    public ChallengeMgr getChallengeMgr() {
        return challengeMgr;
    }

    public void setChallengeMgr(ChallengeMgr challengeMgr) {
        this.challengeMgr = challengeMgr;
    }

    private static void setInstance(PlayerFights instance) {
        PlayerFights.instance = instance;
    }
}
