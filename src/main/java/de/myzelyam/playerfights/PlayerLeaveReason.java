package de.myzelyam.playerfights;

public enum PlayerLeaveReason {
	GAME_END_NORMAL(ArenaStopReason.GAME_END_NORMAL), PLAYER_LEAVE(
			ArenaStopReason.PLAYER_LEAVE), FORCED_STOP(
					ArenaStopReason.FORCED_STOP);

	PlayerLeaveReason(ArenaStopReason stopReason) {
		this.stopReason = stopReason;
	}

	private final ArenaStopReason stopReason;

	public ArenaStopReason toArenaStopReason() {
		return stopReason;
	}
}
