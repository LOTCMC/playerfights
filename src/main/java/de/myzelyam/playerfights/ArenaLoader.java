package de.myzelyam.playerfights;

import org.bukkit.Location;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ArenaLoader {

    private final PlayerFights plugin;

    public ArenaLoader(PlayerFights plugin) {
        this.plugin = plugin;
    }

    public List<Arena> loadArenas() {
        List<Arena> arenas = new LinkedList<>();
        for (String id : new ArrayList<>(plugin.getDataMgr().getArenaIDs())) {
            arenas.add(loadArena(id));
        }
        return arenas;
    }

    public Arena loadArena(String id) {
        id = id.toLowerCase();
        String displayId = plugin.getDataMgr().getArenaDisplayId(id);
        String map = plugin.getDataMgr().getMapOfArena(id);
        List<Location> spawnLocations = plugin.getDataMgr().getSpawnLocationsOfArena(id);
        if (spawnLocations != null && spawnLocations.isEmpty())
            spawnLocations = null;
        ArenaType type = plugin.getDataMgr().getArenaType(id);
        return plugin.getArenaMgr().addArenaObject(id, displayId, type, spawnLocations,
                map);
    }
}
