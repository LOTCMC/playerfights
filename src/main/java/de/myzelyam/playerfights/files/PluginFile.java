package de.myzelyam.playerfights.files;

public interface PluginFile<CT> {

	public String getName();

	public void reload();

	public CT getConfig();

	public void save();
}
