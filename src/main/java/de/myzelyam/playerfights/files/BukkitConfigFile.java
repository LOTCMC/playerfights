package de.myzelyam.playerfights.files;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.myzelyam.playerfights.PlayerFights;

public class BukkitConfigFile implements PluginFile<FileConfiguration> {

	public BukkitConfigFile(String name) {
		this.name = (name + ".yml");
		setup();
	}

	private PlayerFights plugin = PlayerFights.getInstance();

	private String name;

	private File file;

	private FileConfiguration fileConfiguration;

	@Override
	public String getName() {
		return name;
	}

	private void setup() {
		this.file = new File(plugin.getDataFolder(), name);
		save();
	}

	@Override
	public void reload() {
		fileConfiguration = YamlConfiguration.loadConfiguration(file);
	}

	@Override
	public FileConfiguration getConfig() {
		if (fileConfiguration == null) {
			this.reload();
		}
		return fileConfiguration;
	}

	@Override
	public void save() {
		if (!file.exists()) {
			plugin.saveResource(name, false);
		}
	}
}
