package de.myzelyam.playerfights;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers.WorldBorderAction;

/* Unused */
@Deprecated
public class BloodScreenMgr {

	private final Arena arena;

	private final Set<Player> playerSet = new HashSet<>();

	public BloodScreenMgr(Arena arena) {
		this.arena = arena;
	}

	public Arena getArena() {
		return arena;
	}

	public void addHit(Player p) {
		if (!playerSet.add(p))
			return;
		PacketContainer packet = new PacketContainer(
				PacketType.Play.Server.WORLD_BORDER);
		packet.getModifier().writeDefaults();
		// set action to initialize
		packet.getWorldBorderActions().write(0, WorldBorderAction.INITIALIZE);
		// set center to player's position
		packet.getDoubles().write(0, p.getLocation().getX());
		packet.getDoubles().write(1, p.getLocation().getZ());
		// set border radius to 100 (no arena should be bigger than that)
		packet.getDoubles().write(3, 100d);
		// set warning distance to 100 as well (effect everywhere)
		packet.getIntegers().write(2, 100);
		// set warning time to Integer.MAX_VALUE (effect stays)
		packet.getIntegers().write(1, Integer.MAX_VALUE);
		// send
		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(p, packet);
		} catch (InvocationTargetException e) {
			throw new RuntimeException("Cannot send packet " + packet, e);
		}
	}

	public void removeEffect(Player p) {
		if (!playerSet.remove(p))
			return;
		PacketContainer packet = new PacketContainer(
				PacketType.Play.Server.WORLD_BORDER);
		packet.getModifier().writeDefaults();
		// set action to initialize
		packet.getWorldBorderActions().write(0, WorldBorderAction.INITIALIZE);
		// reset center
		packet.getDoubles().write(0,
				p.getWorld().getWorldBorder().getCenter().getX());
		packet.getDoubles().write(1,
				p.getWorld().getWorldBorder().getCenter().getY());
		// reset radius
		packet.getDoubles().write(3, p.getWorld().getWorldBorder().getSize());
		// reset warning distance
		packet.getIntegers().write(2,
				p.getWorld().getWorldBorder().getWarningDistance());
		// reset warning time
		packet.getIntegers().write(1,
				p.getWorld().getWorldBorder().getWarningTime());
		// send
		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(p, packet);
		} catch (InvocationTargetException e) {
			throw new RuntimeException("Cannot send packet " + packet, e);
		}
	}
}
