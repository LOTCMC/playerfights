package de.myzelyam.playerfights;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class DamageActionBarMgr {

    public DamageActionBarMgr() {
        if (plugin.getConfig().getBoolean("EnableActionbars"))
            startTask();
    }

    public static final String ACTIONBAR = ChatColor.RED + "" + ChatColor.BOLD + "%p" + ChatColor.WHITE +
            "" + ChatColor.BOLD + " - " + ChatColor.RED + "" + ChatColor.BOLD + "%health hp";

    private PlayerFights plugin = PlayerFights.getInstance();

    private boolean taskStarted = false;

    private boolean isInArena(Player p) {
        return plugin.getArenaMgr().getArena(p) != null;
    }

    private void startTask() {
        if (taskStarted)
            return;
        taskStarted = true;
        new BukkitRunnable() {

            @Override
            public void run() {
                for (Player p : Bukkit.getOnlinePlayers()) {
                    if (isInArena(p)) {
                        Player nearest = findNearestPlayer(p);
                        if (nearest == null) {
                            continue;
                        }
                        ExtraFeatures.sendActionBar(p,
                                ACTIONBAR.replace("%p",
                                        nearest.getName())
                                        .replace("%health",
                                                ((int) nearest
                                                        .getHealth())
                                                        + ""));
                    }
                }
            }
        }.runTaskTimerAsynchronously(plugin, 0, 2);
    }

    public void update(Arena a) {
        for (Player p : a) {
            Player nearest = findNearestPlayer(p);
            if (nearest == null) {
                continue;
            }
            ExtraFeatures.sendActionBar(p,
                    ACTIONBAR.replace("%p", nearest.getName()).replace(
                            "%health",
                            ((int) nearest.getHealth()) + ""));
        }
    }

    private Player findNearestPlayer(Player p) {
        Player nearestPlayer = null;
        double lowestDistance = Double.MAX_VALUE;
        for (Entity e : p.getNearbyEntities(20d, 20d, 20d)) {
            if (!(e instanceof Player))
                continue;
            Player p2 = (Player) e;
            if (p2.getGameMode() != GameMode.SURVIVAL)
                continue;
            double distance = p.getLocation().distanceSquared(p2.getLocation());
            if (distance < lowestDistance) {
                nearestPlayer = p2;
                lowestDistance = distance;
            }
        }
        return nearestPlayer;
    }
}
