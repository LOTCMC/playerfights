package de.myzelyam.playerfights.duel;

import com.sun.istack.internal.Nullable;
import de.myzelyam.playerfights.*;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.material.MaterialData;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.logging.Level;

public class DuelArena extends Arena {

    private PlayerFreezer freezer;

    private PlayerDataMgr inventoryMgr;

    private ScoreboardMgr scoreboardMgr;

    private KitMgr kitMgr;

    public int round = 1, selectedRounds = 1;

    public Map<Player, Integer> roundWins = new HashMap<>();

    public DuelArena(String id, String displayId, @Nullable List<Location> locations,
                     @Nullable String map, ArenaMgr arenaMgr) {
        super(id, displayId, ArenaType.DUEL, map, locations, arenaMgr);
        setLocations(locations);
        freezer = new PlayerFreezer(getPlugin(), this);
        inventoryMgr = new PlayerDataMgr(getPlugin());
        scoreboardMgr = new ScoreboardMgr(this);
        kitMgr = new KitMgr(this);
        if (!(locations == null || locations.isEmpty() || locations.size() < 2) && map != null)
            setState(ArenaState.WAITING);
        else
            setState(ArenaState.DISABLED);
    }

    @Override
    public boolean allowPublicJoins() {
        // only allow players to join using the challenge command and not the
        // join one
        return false;
    }

    @Override
    public void startCountdown() {
        // state
        setState(ArenaState.STARTING_FULL);
        // set default countdown
        setStartCountdown(START_COUNTDOWN_TIME);
        // start countdown
        startCountdownTask = new BukkitRunnable() {

            @Override
            public void run() {
                // check if low enough to start
                if (getStartCountdown() <= 0) {
                    startNextRound();
                    cancel();
                    return;
                }
                // send msg
                sendCountdownMessage(getStartCountdown(), true);
                // update scoreboard
                scoreboardMgr.updateInformation();
                // decrease cd
                setStartCountdown(getStartCountdown() - 1);
            }
        }.runTaskTimer(getPlugin(), 0, 20);
        //
    }

    public void startNextRound() {
        if (getState() == ArenaState.DISABLED)
            return;
        // msg
        sendArenaMessageWithPrefix(ChatColor.RED + "Round " + ChatColor.GOLD + "" + round + "" + ChatColor.RED + "/" + ChatColor.GOLD + "" + selectedRounds + "" + ChatColor.RED + ", Fight!");
        // state
        setState(ArenaState.INGAME);
        // unfreeze
        freezer.removeAll();
        // gametime task
        if (gameCountdownTask == null) {
            setGameTime(MAX_GAME_TIME);
            gameCountdownTask = new BukkitRunnable() {

                @Override
                public void run() {
                    // check if low enough to start
                    if (getGameTime() <= 0 && getState() != ArenaState.ENDING) {
                        forceEnd(ArenaStopReason.FORCED_STOP);
                        cancel();
                        return;
                    }
                    // send msg
                    sendCountdownMessage(getGameTime(), false);
                    // update scoreboard
                    scoreboardMgr.updateInformation();
                    // decrease cd
                    setGameTime(getGameTime() - 1);
                }
            }.runTaskTimer(getPlugin(), 0, 20);
        }
    }

    private Location getRandomLoc(List<Location> usedLocations) {
        if (getLocations() == null || getLocations().size() < 2)
            return null;
        List<Location> locations = new LinkedList<>(getLocations());
        locations.removeAll(usedLocations);
        if (locations.isEmpty()) {
            return null;
        } else if (locations.size() < 2)
            for (Location loc : locations)
                return loc;
        int size = locations.size();
        int random = new Random().nextInt(size);
        return locations.get(random);
    }

    @Override
    public void initiateEnd(Player winner, Player loser,
                            ArenaStopReason reason) {
        // update scoreboard
        if (loser != null)
            getScoreboardMgr().updateInformation(loser);
        // display titles
        if (loser != null)
            ExtraFeatures.sendTitle(loser, "" + ChatColor.RED + "" + ChatColor.BOLD + "Defeat!", null, 5, 5, 5);
        ExtraFeatures.sendTitle(winner, "" + ChatColor.GREEN + "" + ChatColor.BOLD + "Victory!", null, 5, 5, 5);
        sendArenaMessageWithPrefix("" + ChatColor.GOLD + "" + ChatColor.BOLD + "" + winner.getName()
                + "" + ChatColor.GOLD + "" + ChatColor.BOLD + " won the duel " + ChatColor.RESET + "" + ChatColor.GOLD + "(" + ChatColor.GREEN + "" + roundWins.get(winner) + "" + ChatColor.GOLD + ":" + ChatColor.RED + ""
                + roundWins.get(loser) + "" + ChatColor.GOLD + ") " + ChatColor.GOLD + "(" + ChatColor.YELLOW + ""
                + ((int) (Math.max(winner.getHealth(), 1))) + "" + ChatColor.GOLD + "hp)!");
        WinAnnouncementMessages.announce(winner, loser,
                (int) winner.getHealth(), this);
        // normalize players once again
        for (Player p : this)
            normalizePlayer(p);
        // state
        setState(ArenaState.ENDING);
        // save win and defeat
        getPlugin().getDataMgr().addWinToPlayer(winner.getUniqueId(),
                ArenaType.DUEL);
        if (loser != null)
            getPlugin().getDataMgr().addDefeatToPlayer(loser.getUniqueId(),
                    ArenaType.DUEL);
        // start win task
        setWinTime(WIN_TIME);
        winCountdownTask = new BukkitRunnable() {

            @Override
            public void run() {
                // restart and reset
                if (getWinTime() <= 0) {
                    cancel();
                    forceEnd(ArenaStopReason.GAME_END_NORMAL);
                    return;
                }
                getScoreboardMgr().updateInformation();
                setWinTime(getWinTime() - 1);
            }
        }.runTaskTimer(getPlugin(), 0, 20);
    }

    @Override
    public void forceEnd(ArenaStopReason reason) {
        // kick players
        for (Player p : new HashSet<>(this.getPlayers()))
            removePlayer(p, reason.toPlayerLeaveReason());
        // reset
        resetArena();
    }

    public void addPlayers(Player p1, Player p2, int amountOfFights) {
        addPlayer(p1);
        addPlayer(p2);
        this.selectedRounds = amountOfFights;
    }

    private void startArenaProcess() {
        //
        scoreboardMgr = new ScoreboardMgr(this);
        // add roundwins default
        for (Player p : this)
            roundWins.put(p, 0);
        // tp
        List<Location> usedLocs = new LinkedList<>();
        for (Player p : this) {
            Location loc = getRandomLoc(usedLocs);
            if (loc != null) {
                boolean success = p.teleport(loc, TeleportCause.PLUGIN);
                if (!success) {
                    sendArenaMessageWithPrefix(
                            "" + ChatColor.RED + "Can't start duel since the teleportation of one player failed.");
                    forceEnd(ArenaStopReason.FORCED_STOP);
                    return;
                }
                usedLocs.add(loc);
            } else
                Bukkit.getLogger().log(Level.SEVERE,
                        "[PlayerFights] Couldn't find enough spawn locations for duel arena "
                                + this.getDisplayId() + ", not tp'ing!");
        }
        // freeze
        for (Player p : getPlayers())
            freezer.addPlayer(p);
        // kits
        kitMgr.start();
        // don't start countdown, the kit mgr does that
    }

    @Override
    public void addPlayer(Player p) {
        if (p == null)
            throw new IllegalArgumentException("Player cannot be null");
        if (getState() == ArenaState.DISABLED) {
            p.sendMessage("" + ChatColor.BLUE + "[Duel] " + ChatColor.RED + "This arena is not ready for a duel!");
            return;
        }
        if (getPlayers().contains(p)) {
            p.sendMessage("" + ChatColor.BLUE + "[Duel] " + ChatColor.RED + "You are already in this arena!");
            return;
        }
        if (getState() == ArenaState.INGAME
                || getState() == ArenaState.ENDING) {
            p.sendMessage(
                    "" + ChatColor.BLUE + "[Duel] " + ChatColor.RED + "You cannot join this arena since it's ingame right now!");
            return;
        }
        // add to set
        getPlayers().add(p);
        // control inv
        inventoryMgr.savePlayerData(p);
        // normalize player
        normalizePlayer(p);
        // send msg
        p.sendMessage("" + ChatColor.BLUE + "[Duel] " + ChatColor.GOLD + "You joined the arena!");
        // already two players? start
        if (getPlayers().size() >= 2) {
            setState(ArenaState.WAITING_FULL);
            // start process
            startArenaProcess();
        }
    }

    @Override
    public void removePlayer(Player p, PlayerLeaveReason reason) {
        if (reason == null)
            reason = PlayerLeaveReason.PLAYER_LEAVE;
        if (p == null)
            throw new IllegalArgumentException("Player cannot be null");
        if (!getPlayers().contains(p)) {
            p.sendMessage(ChatColor.BLUE + "[Duel] " + ChatColor.RED + "You are not in this arena!");
            return;
        }
        // remove from set
        getPlayers().remove(p);
        // send leave msg
        switch (reason) {
            case PLAYER_LEAVE:
                if (getState() != ArenaState.ENDING)
                    p.sendMessage(ChatColor.BLUE + "[Duel] " + ChatColor.RED + "You left the arena and lost the duel!");
                else
                    p.sendMessage(ChatColor.BLUE + "[Duel] " + ChatColor.GOLD + "You left the arena!");
                sendArenaMessageWithPrefix(ChatColor.GOLD + "" + p.getName() + "" + ChatColor.GOLD + " left the arena!");
                break;
            case FORCED_STOP:
                p.sendMessage(ChatColor.BLUE + "[Duel] " + ChatColor.RED + "The arena has been forced to stop!");
                break;
            case GAME_END_NORMAL:
                break;
        }
        // handle leave
        handleLeave(p, reason);
        // normalize
        normalizePlayer(p);
        // restore data
        inventoryMgr.restorePlayerData(p);
        p.updateInventory();
        // freezer
        freezer.removePlayer(p);
        // sb
        p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
    }

    private void handleLeave(Player p, PlayerLeaveReason reason) {
        // don't handle if already over
        if (getState() == ArenaState.ENDING)
            return;
        // stop countdowns if the player left before end
        if (getStartCountdown() != START_COUNTDOWN_TIME) {
            freezer.removeAll();
            stopStartCountdown();
        }
        stopGameTimer();
        //
        switch (reason) {
            case FORCED_STOP: // <- don't handle, arena is already restarting
                break;
            case PLAYER_LEAVE:
                initiateEnd(getOtherPlayer(p), p, reason.toArenaStopReason());
                break;
            case GAME_END_NORMAL:
                initiateEnd(getOtherPlayer(p), p, reason.toArenaStopReason());
                break;
        }
    }

    @Override
    public void resetArena() {
        stopGameTimer();
        stopStartCountdown();
        stopWinTimer();
        freezer.removeAll();
        round = 1;
        selectedRounds = 1;
        roundWins.clear();
        scoreboardMgr = null;
        kitMgr.reset();
        super.resetArena();
    }

    public Player getOtherPlayer(Player p1) {
        for (Player p2 : getPlayers())
            if (p1 != p2)
                return p2;
        return null;
    }

    @Override
    public void onKill(Player killed, @Nullable Player killer) {
        if (killer == null)
            killer = getOtherPlayer(killed);
        // awesome wrecked sound & effect
        for (Player p : this) {
            for (int i = 0; i < 10; i++)
                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_BIG_FALL, 200f, 1);
            for (int i = 0; i < 10; i++)
                p.playEffect(killed.getLocation(), Effect.TILE_BREAK,
                        new MaterialData(Material.REDSTONE_BLOCK));
        }
        // round wins
        roundWins.put(killer, roundWins.get(killer) + 1);
        // end game or start next round
        // limit exceeded, the one who died has to be the loser, otherwise the
        // arena would've ended beforehand anyway
        if (round >= selectedRounds)
            initiateEnd(getOtherPlayer(killed), killed,
                    ArenaStopReason.GAME_END_NORMAL);
            // didn't exceed selected limit
        else {
            // one player (must be killer) got more wins than half of the
            // Math.ceil of the selected rounds
            for (Player p : this) {
                if ((double) roundWins.get(p) >= Math
                        .ceil(selectedRounds / 2.0)) {
                    initiateEnd(getOtherPlayer(killed), killed,
                            ArenaStopReason.GAME_END_NORMAL);
                    return;
                }
            }
            // no win, start next round
            round++;
            // reset players a bit
            int health = (int) killer.getHealth();
            if (health < 1)
                health = 1;
            List<Location> usedLocs = new LinkedList<>();
            for (Player p : this) {
                Location loc = getRandomLoc(usedLocs);
                if (loc == null) {
                    Bukkit.getLogger().log(Level.SEVERE,
                            "[PlayerFights] (Duel) Not enough spawn locs, errors expected!");
                }
                usedLocs.add(loc);
                p.teleport(loc, TeleportCause.PLUGIN);
                normalizePlayer(p);
                kitMgr.giveKitBack(p);
            }
            freezer.addPlayer(getPlayers());
            sendArenaMessageWithPrefix(killer.getName() + "" + ChatColor.GOLD + " won round " + ChatColor.YELLOW + ""
                    + (getRound() - 1) + "" + ChatColor.GOLD + " with " + ChatColor.YELLOW + "" + health + "" + ChatColor.GOLD + "hp left!");
            ExtraFeatures.sendTitle(
                    killed, "" + ChatColor.RED + "" + ChatColor.BOLD + "Defeat! " + ChatColor.RESET + "" + ChatColor.GOLD + "(" + ChatColor.GREEN + "" + roundWins.get(killed)
                            + "" + ChatColor.GOLD + "/" + ChatColor.RED + "" + roundWins.get(killer) + "" + ChatColor.GOLD + ")",
                    null, 5, 5, 5);
            ExtraFeatures.sendTitle(
                    killer, "" + ChatColor.GREEN + "" + ChatColor.BOLD + "Victory! " + ChatColor.RESET + "" + ChatColor.GOLD + "(" + ChatColor.GREEN + "" + roundWins.get(killer)
                            + "" + ChatColor.GOLD + "/" + ChatColor.RED + "" + roundWins.get(killed) + "" + ChatColor.GOLD + ")",
                    null, 5, 5, 5);
            // start countdown once again
            stopStartCountdown();
            startCountdown();
        }
    }

    @Override
    public void onDamage(Player damaged, @Nullable Player damager) {
        getScoreboardMgr().updateInformation();
    }

    public ScoreboardMgr getScoreboardMgr() {
        return scoreboardMgr;
    }

    @Override
    public boolean isReadyForUse() {
        return (getMap() != null && getLocations() != null && getLocations().size() > 1);
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    @Override
    public String[] getCheckCriterions() {
        boolean mapReady = getMap() != null;
        boolean spawnLocsReady = getLocations() != null && getLocations().size() > 1;
        int spawnLocsAmount = getLocations() == null ? 0 : getLocations().size();
        return new String[]{
                "" + ChatColor.YELLOW + "Map ready: " + ChatColor.GOLD + "" + mapReady
                        + (mapReady ? " (" + getMap() + ")" : ""),
                "" + ChatColor.YELLOW + "Spawn Locations ready: " + ChatColor.GOLD + "" + spawnLocsReady + " " + ChatColor.YELLOW + ""
                        + (spawnLocsReady ? "(" + spawnLocsAmount + ")"
                        : "(" + spawnLocsAmount + "<2)")};
    }
}
