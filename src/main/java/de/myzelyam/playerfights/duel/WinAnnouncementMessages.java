package de.myzelyam.playerfights.duel;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.myzelyam.playerfights.ArenaType;
import de.myzelyam.playerfights.PlayerFights;
import org.bukkit.ChatColor;

public abstract class WinAnnouncementMessages {

	public static void announce(Player winner, Player loser, int winnerHealth,
			DuelArena a) {
		String roundStats = "(" + ChatColor.GREEN + "" + a.roundWins.get(winner) + "" + ChatColor.GOLD + ":" + ChatColor.RED + ""
				+ a.roundWins.get(loser) + "" + ChatColor.GOLD + ")";
		String prefix = PlayerFights.getInstance().getPrefix(ArenaType.DUEL);
		if (winnerHealth < 1)
			winnerHealth = 1;
		if (a.selectedRounds < 2) {
			if (winnerHealth >= 16) {
				broadcast(prefix + winner.getName() + "" + ChatColor.YELLOW + " absolutely annihilated "
						+ loser.getName() + "" + ChatColor.YELLOW + " in a duel with " + ChatColor.GOLD + ""
						+ winnerHealth + "" + ChatColor.YELLOW + "hp left!");
			} else if (winnerHealth >= 13) {
				broadcast(prefix + winner.getName() + "" + ChatColor.YELLOW + " destroyed " + loser.getName()
						+ "" + ChatColor.YELLOW + " in a duel with " + ChatColor.GOLD + "" + winnerHealth + "" + ChatColor.YELLOW + "hp left!");
			} else if (winnerHealth >= 9) {
				broadcast(prefix + winner.getName() + "" + ChatColor.YELLOW + " wrecked " + loser.getName()
						+ "" + ChatColor.YELLOW + " in a duel with " + ChatColor.GOLD + "" + winnerHealth + "" + ChatColor.YELLOW + "hp left!");
			} else if (winnerHealth >= 6) {
				broadcast(prefix + winner.getName() + "" + ChatColor.YELLOW + " killed " + loser.getName()
						+ "" + ChatColor.YELLOW + " in a duel with " + ChatColor.GOLD + "" + winnerHealth + "" + ChatColor.YELLOW + "hp left!");
			} else if (winnerHealth >= 3) {
				broadcast(prefix + winner.getName() + "" + ChatColor.YELLOW + " won a close duel against "
						+ loser.getName() + "" + ChatColor.YELLOW + " with " + ChatColor.GOLD + "" + winnerHealth
						+ "" + ChatColor.YELLOW + "hp left!");
			} else if (winnerHealth >= 1) {
				broadcast(prefix + winner.getName()
						+ "" + ChatColor.YELLOW + " won an extremely close duel against "
						+ loser.getName() + "" + ChatColor.YELLOW + " with " + ChatColor.GOLD + "" + winnerHealth
						+ "" + ChatColor.YELLOW + "hp left!");
			}
		} else {
			String number = "";
			switch (a.selectedRounds) {
			case 1:
				number = "One";
				break;
			case 2:
				number = "Two";
				break;
			case 3:
				number = "Three";
				break;
			case 4:
				number = "Four";
				break;
			case 5:
				number = "Five";
				break;
			case 9:
				number = "Nine";
				break;
			}
			// the rounds devided by the least amount of rounds possible without
			// quit
			/*double customWinDouble = (a.round)
					/ (Math.ceil(a.selectedRounds / 2.0));
			String word = "killed";
			if (customWinDouble <= 1.0) {
				word = "absolutely annihilated";
			} else if (customWinDouble <= 1.34) {
				word = "wrecked";
			} else if (customWinDouble <= 1.67) {
				word = "won a close duel against";
			}*/
			broadcast(prefix + winner.getName() + " " + ChatColor.YELLOW + "killed " + loser.getName()
					+ "" + ChatColor.YELLOW + " in a Best-of-" + number + " duel! " + roundStats);
		}
	}

	private static void broadcast(String msg) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('§', msg));
		}
	}
}
