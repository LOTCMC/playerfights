package de.myzelyam.playerfights;

import com.sun.istack.internal.Nullable;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;

public abstract class Arena implements Iterable<Player> {

    // 5 seconds
    public static final int START_COUNTDOWN_TIME = 5;

    // 20 minutes
    public static final int MAX_GAME_TIME = 2 * 600;

    // 7 seconds
    public static final int WIN_TIME = 7;

    // announcement times in countdowns
    public static final List<Integer> COUNTDOWN_ANNOUNCEMENTS = Arrays.asList(1,
            2, 3, 4, 5, 10, 30, 45, 60, 120, 600, 1400, 2400, 4800, 6000);

    protected BukkitTask startCountdownTask;

    protected BukkitTask gameCountdownTask;

    protected BukkitTask winCountdownTask;

    private int startCountdown, gameCountdown, winCountdown;

    private final PlayerFights plugin;

    private final ArenaMgr arenaMgr;

    private final String id, displayId;

    private String map;

    private final ArenaType type;

    private Set<Player> players = new HashSet<>();

    private List<Location> locations;

    private ArenaState state;

    public Arena(String id, String displayId, ArenaType type, String map,
            List<Location> locations, ArenaMgr arenaMgr) {
        this.id = id;
        this.displayId = displayId;
        this.locations = locations;
        this.arenaMgr = arenaMgr;
        this.map = map;
        this.type = type;
        this.plugin = getArenaMgr().getPlugin();
        setState(ArenaState.WAITING);
    }

    public boolean allowPublicJoins() {
        return true;
    }

    public abstract String[] getCheckCriterions();

    public abstract boolean isReadyForUse();

    public abstract void startCountdown();

    public abstract void addPlayer(Player p);

    public abstract void removePlayer(Player p, PlayerLeaveReason reason);

    public abstract void initiateEnd(Player winner, Player loser,
            ArenaStopReason reason);

    public abstract void forceEnd(ArenaStopReason reason);

    public void onKill(Player killed, @Nullable Player killer) {
    }

    public void onDamage(Player damaged, @Nullable Player damager) {
    }

    public ArenaType getType() {
        return type;
    }

    public void resetArena() {
        getPlayers().clear();
        state = ArenaState.WAITING;
    }

	public void sendArenaMessage(String msg) {
        for (Player p : this) {
            p.sendMessage(msg);
        }
    }

    public void sendArenaMessageWithPrefix(String msg) {
        for (Player p : this) {
            p.sendMessage( ChatColor.BLUE + "[" + getType().makeNice() + "] " + ChatColor.YELLOW + "" + msg);
        }
    }

    protected void normalizePlayer(Player p) {
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        p.setFoodLevel(20);
        p.setHealth(20d);
        p.setAllowFlight(false);
        p.setLevel(0);
        p.setFireTicks(0);
        p.setFallDistance(0);
        p.setGameMode(GameMode.SURVIVAL);
        p.resetMaxHealth();
        for (PotionEffect e : p.getActivePotionEffects()) {
            p.removePotionEffect(e.getType());
        }
    }

    @Override
    public Iterator<Player> iterator() {
        return players.iterator();
    }

    public void stopStartCountdown() {
        if (startCountdownTask != null) {
            startCountdownTask.cancel();
        }
        startCountdownTask = null;
        setStartCountdown(START_COUNTDOWN_TIME);
    }

    public void stopGameTimer() {
        if (gameCountdownTask != null) {
            gameCountdownTask.cancel();
        }
        gameCountdownTask = null;
        setGameTime(MAX_GAME_TIME);
    }

    public void stopWinTimer() {
        if (winCountdownTask != null) {
            winCountdownTask.cancel();
        }
        winCountdownTask = null;
        setWinTime(WIN_TIME);
    }

    protected void sendCountdownMessage(int time, boolean isStartCd) {
        if (!COUNTDOWN_ANNOUNCEMENTS.contains(time)) {
            return;
        }
        if (isStartCd) {
            sendArenaMessageWithPrefix("The fight starts in " + ChatColor.GOLD + ""
                    + GeneralUtils.getDurationBreakdown(time) + "" + ChatColor.YELLOW + "!");
        } else {
            sendArenaMessageWithPrefix("The arena ends in " + ChatColor.GOLD + ""
                    + GeneralUtils.getDurationBreakdown(time) + "" + ChatColor.YELLOW + "!");
        }
    }

	// getters § setters
    public String getId() {
        return id;
    }

    public String getDisplayId() {
        return displayId;
    }

    public ArenaMgr getArenaMgr() {
        return arenaMgr;
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    public ArenaState getState() {
        return state;
    }

    public void setState(ArenaState state) {
        this.state = state;
    }

    public PlayerFights getPlugin() {
        return plugin;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public int getStartCountdown() {
        return startCountdown;
    }

    public void setStartCountdown(int startCountdown) {
        this.startCountdown = startCountdown;
    }

    public int getGameTime() {
        return gameCountdown;
    }

    public void setGameTime(int gameTime) {
        this.gameCountdown = gameTime;
    }

    public int getWinTime() {
        return winCountdown;
    }

    public void setWinTime(int winTime) {
        this.winCountdown = winTime;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}
