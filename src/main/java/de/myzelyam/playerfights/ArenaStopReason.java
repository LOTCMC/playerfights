package de.myzelyam.playerfights;

public enum ArenaStopReason {
	GAME_END_NORMAL(PlayerLeaveReason.GAME_END_NORMAL), PLAYER_LEAVE(
			PlayerLeaveReason.PLAYER_LEAVE), FORCED_STOP(
					PlayerLeaveReason.FORCED_STOP);

	ArenaStopReason(PlayerLeaveReason leaveReason) {
		this.leaveReason = leaveReason;
	}

	private final PlayerLeaveReason leaveReason;

	public PlayerLeaveReason toPlayerLeaveReason() {
		return leaveReason;
	}
}
