package de.myzelyam.playerfights;

import com.sun.istack.internal.Nullable;
import de.myzelyam.playerfights.duel.DuelArena;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashSet;

public class ScoreboardMgr {

    private PlayerFights plugin;

    private DuelArena arena;

    private Scoreboard bukkitScoreboard;

    private Objective bukkitObjective;

    public ScoreboardMgr(DuelArena arena) {
        this.plugin = arena.getPlugin();
        this.arena = arena;
        String type = arena.getType().makeNice();
        bukkitScoreboard = plugin.getServer().getScoreboardManager()
                .getNewScoreboard();
        if (bukkitScoreboard.getObjective(type + "SB") == null)
            bukkitScoreboard.registerNewObjective(type + "SB", "dummy");
        bukkitObjective = bukkitScoreboard.getObjective(type + "SB");
        bukkitObjective.setDisplayName("" + ChatColor.GOLD + ">      > " + ChatColor.BLUE + "" + type + " " + ChatColor.GOLD + "<      <");
        bukkitObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    private void display() {
        for (Player p : arena)
            p.setScoreboard(bukkitScoreboard);
    }

    public void updateInformation(@Nullable Player dead) {
        display();
        byte count = 1;
        if (arena.getState() != ArenaState.INGAME
                && arena.getState() != ArenaState.ENDING) {
            // update countdown timer
            addScore(
                    "" + ChatColor.YELLOW + "Starting in " + ChatColor.GOLD + "" + GeneralUtils
                            .getDurationBreakdown(arena.getStartCountdown()),
                    count++, "" + ChatColor.YELLOW + "Starting in " + ChatColor.GOLD + "", false);
        } else {
            if (arena.getState() == ArenaState.INGAME) {
                // remove start cd if required
                for (String key : new HashSet<>(
                        bukkitScoreboard.getEntries())) {
                    if (key.contains("" + ChatColor.YELLOW + "Starting in " + ChatColor.GOLD + ""))
                        bukkitScoreboard.resetScores(key);
                }
                addScore("" + ChatColor.YELLOW + "Fighting...", count++, "" + ChatColor.YELLOW + "Fighting...", true);
            } else {
                // remove countdown timer if required
                for (String key : new HashSet<>(
                        bukkitScoreboard.getEntries())) {
                    if (key.contains("" + ChatColor.YELLOW + "Starting in " + ChatColor.GOLD + ""))
                        bukkitScoreboard.resetScores(key);
                }
                bukkitScoreboard.resetScores("" + ChatColor.YELLOW + "Fighting...");
                // update end timer
                addScore(
                        "" + ChatColor.YELLOW + "Stopping in " + ChatColor.GOLD + "" + GeneralUtils
                                .getDurationBreakdown(arena.getWinTime()),
                        count++, "" + ChatColor.YELLOW + "Stopping in " + ChatColor.GOLD + "", false);
            }
        }
        // health
        for (Player p : arena) {
            addScore(ChatColor.AQUA + "" + p.getName() + "" + ChatColor.GOLD + " > "
                            + (p.getHealth() > 10.0 ? ChatColor.GREEN : ChatColor.RED)
                            + (int) (p == dead ? 0.0 : p.getHealth())
                            + /* round wins */" " + ChatColor.GOLD + "(" + ChatColor.YELLOW + "" + arena.roundWins.get(p) + "" + ChatColor.GOLD + ")",
                    count++, "" + ChatColor.AQUA + "" + p.getName() + "" + ChatColor.GOLD + " > ", false);
        }
        // round
        addScore("" + ChatColor.YELLOW + "Round " + ChatColor.GOLD + "" + arena.getRound() + "" + ChatColor.RED + "/" + ChatColor.GOLD + ""
                + arena.selectedRounds, count, "" + ChatColor.YELLOW + "Round " + ChatColor.GOLD + "", false);
    }

    public void updateInformation() {
        updateInformation(null);
    }

    private void addScore(String string, byte count, String id, boolean exact) {
        for (String entry : bukkitScoreboard.getEntries()) {
            if ((entry.contains(id) && !exact) || (exact && entry.equals(id))) {
                if (!entry.equals(string)) {
                    // clear old information
                    bukkitScoreboard.resetScores(entry);
                    // update information
                    bukkitObjective.getScore(string).setScore(count);
                }
                return;
            }
        }
        // add new information
        bukkitObjective.getScore(string).setScore(count);
    }

    public void removeScoreboard() {
        for (Player player : arena)
            player.setScoreboard(plugin.getServer().getScoreboardManager()
                    .getNewScoreboard());
    }

    public void removeScoreboard(Player p) {
        p.setScoreboard(
                plugin.getServer().getScoreboardManager().getNewScoreboard());
    }
}
