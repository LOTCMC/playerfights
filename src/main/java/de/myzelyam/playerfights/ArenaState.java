package de.myzelyam.playerfights;

public enum ArenaState {
	DISABLED(false), WAITING(true), WAITING_FULL(false), STARTING(
			true), STARTING_FULL(false), INGAME(false), ENDING(false);

	private boolean allowsJoin;

	ArenaState(boolean allowsJoin) {
		this.setAllowsJoin(allowsJoin);
	}

	public boolean allowsJoin() {
		return allowsJoin;
	}

	private void setAllowsJoin(boolean allowsJoin) {
		this.allowsJoin = allowsJoin;
	}
}
