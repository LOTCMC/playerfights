package de.myzelyam.playerfights;

import com.sun.istack.internal.Nullable;
import de.myzelyam.playerfights.duel.DuelArena;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.*;

public class ArenaMgr {

	private final PlayerFights plugin;

	private final Set<Arena> arenas;

	public ArenaMgr(PlayerFights plugin) {
		this.plugin = plugin;
		arenas = new HashSet<>();
	}

	public Arena getArena(String id) {
		for (Arena a : arenas) {
			if (a.getId().equalsIgnoreCase(id))
				return a;
		}
		return null;
	}

	public Arena getArena(Player p) {
		for (Arena a : arenas) {
			if (a.getPlayers().contains(p))
				return a;
		}
		return null;
	}

	public boolean isInGame(Player p) {
		return getArena(p) != null;
	}

	public Arena addArenaObject(String id, String displayId, ArenaType type,
			List<Location> locs, String mapId) {
		Arena a = null;
		if (type == ArenaType.DUEL)
			a = new DuelArena(id, displayId, locs, mapId, this);
		if (a == null) {
			throw new IllegalArgumentException("The arena type \""
					+ type.toString() + "\" doesn't appear to exist.");
		}
		arenas.add(a);
		return a;
	}

	public void removeArenaObject(String id) {
		Arena a = getArena(id);
		if (a == null)
			return;
		a.forceEnd(ArenaStopReason.FORCED_STOP);
		arenas.remove(a);
	}

	public Arena createArena(String id, String displayId, ArenaType type,
			@Nullable String mapId, @Nullable List<Location> spawnLocs) {
		id = id.toLowerCase();
		plugin.getDataMgr().addArenaIDToList(id);
		plugin.getDataMgr().createArenaSection(id, displayId, type);
		if (spawnLocs != null)
			plugin.getDataMgr().setSpawnLocsForArena(id, spawnLocs);
		if (mapId != null)
			plugin.getDataMgr().setMapOfArena(id, mapId);
		return addArenaObject(id, displayId, type, spawnLocs, mapId);
	}

	public void deleteArena(String id) {
		id = id.toLowerCase();
		removeArenaObject(id);
		plugin.getDataMgr().removeArenaIDFromList(id);
		plugin.getDataMgr().deleteArenaSection(id);
	}

	public Arena getDuelArenaByMap(String map) {
		for (Arena a : getAllArenas()) {
			if (a.getType() == ArenaType.DUEL
					&& a.getMap().equalsIgnoreCase(map)
					&& (a.getState() == ArenaState.WAITING)) {
				return a;
			}
		}
		return null;
	}

	public DuelArena getRandomDuelArena() {
		List<DuelArena> arenas = new LinkedList<>();
		for (Arena a : getAllArenas()) {
			if (a.getType() == ArenaType.DUEL
					&& a.getState() == ArenaState.WAITING) {
				arenas.add((DuelArena) a);
			}
		}
		if (arenas.isEmpty())
			return null;
		int random = new Random().nextInt(arenas.size());
		return arenas.get(random);
	}

	public Set<Arena> getAllArenas() {
		return arenas;
	}

	public PlayerFights getPlugin() {
		return plugin;
	}
}
