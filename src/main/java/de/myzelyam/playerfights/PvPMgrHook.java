package de.myzelyam.playerfights;

import org.bukkit.entity.Player;

import me.NoChance.PvPManager.PvPlayer;

public abstract class PvPMgrHook {

	public static boolean isInCombat(Player p) {
		PvPlayer pvplayer = PvPlayer.get(p);
		return pvplayer != null && pvplayer.isInCombat();
	}
}
