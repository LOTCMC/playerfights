package de.myzelyam.playerfights;

import de.myzelyam.playerfights.duel.DuelArena;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.projectiles.ProjectileSource;

public class GeneralPlayerListener implements Listener {

    private PlayerFights plugin;

    public GeneralPlayerListener(PlayerFights plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onCommandPreProcessEvent(PlayerCommandPreprocessEvent e) {
        Player p = e.getPlayer();
        if (p.hasPermission("playerfights.allowcmdsingame"))
            return;
        // only deny cmds in arena and in tp phase (because of /tpaccept)
        if (!isInArena(p)
                && !plugin.getChallengeMgr().preTeleportPlayers.contains(p)) {
            return;
        }
        String cmd = e.getMessage().toLowerCase();
        cmd = (cmd.contains(" ") ? cmd.split(" ")[0] : cmd);
        // rem /
        cmd = cmd.substring(1, cmd.length());
        if (!plugin.getConfig().getStringList("WhitelistedCommands")
                .contains(cmd)) {
            if (!(cmd.equalsIgnoreCase("duel")
                    || cmd.equalsIgnoreCase("pfsetup")
                    || cmd.equalsIgnoreCase("pfs"))) {
                e.setCancelled(true);
                p.sendMessage(ChatColor.RED + "You cannot use this command in an arena! Use /spawn to leave.");
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBreak(BlockBreakEvent e) {
        if (isInArena(e.getPlayer())) {
            e.setCancelled(true);
            if (e.getBlock().getType().isSolid())
                if (plugin.getArenaMgr()
                        .getArena(e.getPlayer()) instanceof DuelArena)
                    e.getPlayer()
                            .sendMessage(ChatColor.RED + "Don't kill the blocks, kill "
                                    + ((DuelArena) plugin.getArenaMgr()
                                    .getArena(e.getPlayer()))
                                    .getOtherPlayer(
                                            e.getPlayer())
                                    .getName()
                                    + " instead!");
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlace(BlockPlaceEvent e) {
        if (isInArena(e.getPlayer()))
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onFoodChange(FoodLevelChangeEvent e) {
        if (!(e.getEntity() instanceof Player))
            return;
        if (isInArena((Player) e.getEntity())) {
            e.setFoodLevel(20);
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        if (isInArena(e.getPlayer()))
            e.setCancelled(true);
    }

    @EventHandler
    public void onPotionSplash(PotionSplashEvent e) {
        ProjectileSource ps = e.getPotion().getShooter();
        if (ps == null)
            return;
        if (ps instanceof Player) {
            Player p = (Player) ps;
            Arena a = plugin.getArenaMgr().getArena(p);
            if (a == null)
                return;
            if (a.getState() != ArenaState.INGAME) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onKill(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player))
            return;
        Player p = (Player) e.getEntity();
        // is in arena so idc about plugin compatibility
        if (!isInArena(p)) {
            // remove from tp queue to restore PvPManager compatibility
            if (plugin.getChallengeMgr().preTeleportPlayers.contains(p)) {
                plugin.getChallengeMgr().preTeleportPlayers.remove(p);
            }
            return;
        }
        Arena a = plugin.getArenaMgr().getArena(p);
        // action bars
        plugin.damageActionBarMgr.update(a);
        // is start or end phase?
        if (a.getState() != ArenaState.INGAME) {
            e.setCancelled(true);
            return;
        }
        // did it kill?
        if (e.getFinalDamage() >= p.getHealth()) {
            a.onDamage(p, null);
            a.onKill(p, null);
            e.setCancelled(true);
        } else {
            a.onDamage(p, null);
        }
        //
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if (!isInArena(p))
            return;
        Arena a = plugin.getArenaMgr().getArena(p);
        a.removePlayer(p, PlayerLeaveReason.PLAYER_LEAVE);
    }

    @EventHandler
    public void onQuit(PlayerKickEvent e) {
        Player p = e.getPlayer();
        if (!isInArena(p))
            return;
        Arena a = plugin.getArenaMgr().getArena(p);
        a.sendArenaMessageWithPrefix(ChatColor.RED + "Stopping arena since " + p.getName()
                + " has been kicked from the server.");
        a.removePlayer(p, PlayerLeaveReason.PLAYER_LEAVE);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onTP(PlayerTeleportEvent e) {
        Player p = e.getPlayer();
        if (isInArena(p)) {
            Arena a = plugin.getArenaMgr().getArena(p);
            if (e.getCause() == TeleportCause.COMMAND) {
                if (!e.isCancelled()) {
                    a.removePlayer(p, PlayerLeaveReason.PLAYER_LEAVE);
                }
            } else if (e.getCause() == TeleportCause.ENDER_PEARL) {
                if (!e.getTo().getWorld().getName()
                        .equals(a.getLocations().get(0).getWorld().getName())) {
                    e.setCancelled(true);
                    p.sendMessage(plugin.getPrefix() + ChatColor.DARK_RED + "Don't try to exploit Duel with enderpearls!");
                }
            } else {
                if (!e.getTo().getWorld().getName()
                        .equals(a.getLocations().get(0).getWorld().getName())) {
                    if (!e.isCancelled()) {
                        a.removePlayer(p, PlayerLeaveReason.PLAYER_LEAVE);
                    }
                }
            }
        }
    }

    private boolean isInArena(Player p) {
        return plugin.getArenaMgr().getArena(p) != null;
    }
}
